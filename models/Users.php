<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 * @property int $activate
 */
class Users extends ActiveRecord{
    
    public static function getDb()
    {
        return Yii::$app->db;
    }
    
    public static function tableName()
    {
        return 'users';
    }
    
}
   
    
    
    
//    public static function tableName()
//    {
//        return 'users';
//    }
//
//   
//    public function rules()
//    {
//        return [
//            [['activate'], 'integer'],
//            [['username'], 'string', 'max' => 80],
//            [['password', 'email', 'authKey', 'accessToken'], 'string', 'max' => 255],
//            [['username', 'password'], 'unique', 'targetAttribute' => ['username', 'password']],
//        ];
//    }
//
//  
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'username' => 'Username',
//            'password' => 'Password',
//            'email' => 'Email',
//            'authKey' => 'Auth Key',
//            'accessToken' => 'Access Token',
//            'activate' => 'Activate',
//        ];
//    }

