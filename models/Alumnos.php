<?php

namespace app\models;
use yii\base\Model;


use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int|null $id_escolar
 * @property int|null $expe_centro
 * @property string $passnie
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $f_nac
 * @property string|null $loc_nac
 * @property string|null $prov_nac
 * @property string|null $domicilio
 * @property int|null $cp
 * @property string|null $localidad
 * @property string|null $provincia
 * @property int|null $tel_fijo
 * @property int|null $movil
 * @property int|null $discapacidad
 * @property string|null $centro_ant
 * @property string|null $tit_acceso
 * @property string|null $foto
 * @property string|null $email
 * @property string|null $nacionalidad
 * @property string|null $pais_nac
 *
 * @property Discapacidades $discapacidad0
 * @property Matriculas[] $matriculas
 * @property Responsables[] $responsables
 */
class Alumnos extends \yii\db\ActiveRecord
{
      public $fotoAlumno;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_escolar', 'expe_centro', 'cp', 'tel_fijo', 'movil', 'discapacidad','f_proteccion'], 'integer'],
            [['passnie'],'string','max' => 2],
            [['dni'], 'required'],
            [['f_nac'], 'safe'],
            [['foto'], 'string'],
            [['dni'], 'string', 'max' => 9],
            [['nombre', 'loc_nac', 'prov_nac', 'localidad', 'provincia', 'email'], 'string', 'max' => 100],
            [['apellidos', 'domicilio', 'centro_ant', 'tit_acceso'], 'string', 'max' => 200],
            [['nacionalidad'], 'string', 'max' => 50],
            [['dni'], 'unique'],
            [['discapacidad'], 'exist', 'skipOnError' => true, 'targetClass' => Discapacidades::className(), 'targetAttribute' => ['discapacidad' => 'id']],
            [['observaciones'], 'string'],
        ];
         return [
            [['fotoAlumno'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif'],
           
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_escolar' => 'Nº Escolar',
            'expe_centro' => 'Nº Expediente',
            'passnie' => 'PASS/NIE',
            'dni' => 'DNI',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'f_nac' => 'Fecha Nacimiento',
            'loc_nac' => 'Localidad Nacimiento',
            'prov_nac' => 'Provincia Nacimiento',
            'nacionalidad' => 'Nacionalidad',
            'domicilio' => 'Domicilio',
            'cp' => 'C.P.',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
            'tel_fijo' => 'Teléfono Fijo',
            'movil' => 'Movil',
            'email' => 'Email',
            'discapacidad' => 'Discapacidad',
            'centro_ant' => 'Centro Anterior',
            'tit_acceso' => 'Titulación Acceso',
            'foto' => 'Foto', 
            'f_proteccion' => 'Protección Datos',
            'observaciones'=>'Observaciones',
             
        ];
    }
    
    
//    public function relations()
//    {
//        return array(
//            'matriculas'=>array(self::BELONGS_TO, 'Matriculas(dni_alumno)', 'dni'),
//            'responsables'=>array(self::MANY_MANY, 'Responsables(dni_alumno)', 'dni'),
//            'discapacidades'=>array(self::HAS_MANY, 'Discapacidad', 'discapacidad'),  
//        );
//    }

    /**
     * Gets query for [[Discapacidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscapacidad0()
    {
        return $this->hasOne(Discapacidades::className(), ['id' => 'discapacidad']);
    }
     public function getDiscapacidades()
    {
        return $this->hasOne(Discapacidades::className(), ['id' => 'discapacidad']);
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matriculas::className(), ['dni_alumno' => 'dni']);
    }

    /**
     * Gets query for [[Responsables]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResponsables()
    {
        return $this->hasMany(Responsables::className(), ['dni_alumno' => 'dni']);
    }
    
//      public function getDatosbancarios()
//    {
//        return $this->hasOne(Datosbancarios::className(), ['id_datos_bancarios' => 'id']);
//    }
      
       
     public function afterFind() {
        parent::afterFind();
        $this->f_nac=Yii::$app->formatter->asDate($this->f_nac, 'php:d-m-Y');;
        
    }
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          $this->f_nac=Yii::$app->formatter->asDate($this->f_nac, 'php:Y-m-d');
          //$this->alta= \DateTime::createFromFormat("d/m/Y", $this->alta)->format("Y/m/d");
          return true;
    }
}
