<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "modulosmatricula".
 *
 * @property int $id
 * @property int|null $id_matricula
 * @property int|null $id_modulo
 * @property string|null $estado
 *
 * @property Matriculas $matricula
 * @property Modulosciclo $modulo
 */
class Modulosmatricula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modulosmatricula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_matricula', 'id_modulo'], 'integer'],
            [['estado'], 'string', 'max' => 2],
            [['id_matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Matriculas::className(), 'targetAttribute' => ['id_matricula' => 'id']],
            [['id_modulo'], 'exist', 'skipOnError' => true, 'targetClass' => Modulosciclo::className(), 'targetAttribute' => ['id_modulo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_matricula' => 'Id Matricula',
            'id_modulo' => 'Id Modulo',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Matricula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula()
    {
        return $this->hasOne(Matriculas::className(), ['id' => 'id_matricula']);
    }

    /**
     * Gets query for [[Modulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulosciclo::className(), ['id' => 'id_modulo']);
    }
    public function getModulo0()
    {
        return $this->hasOne(Modulos::className(), ['id' => 'id_modulo']);
    }
    
    
     //creados para que coincidan las tablas con las relaciones en informe dinámico
     public function getMatriculas()
    {
        return $this->hasOne(Matriculas::className(), ['id' => 'id_matricula']);
    }
    public function getModulos()
    {
        return $this->hasOne(Modulosciclo::className(), ['id' => 'id_modulo']);
    }
    //-------------------------------------------------------------------------------
    
    
    
    
    
    public function getUltimoestado($alumno,$modulo){
        $query = new Query();
        $query->select('mo.estado estado')
		->from('modulosmatricula mo')
		->innerJoin('matriculas ma', 'ma.id = mo.id_matricula')
                ->where('mo.id_modulo = '.$modulo)
		->andWhere(['ma.dni_alumno' => $alumno])
                ->orderBy('ma.id','desc')
                ->limit('1');
        $estado = $query->scalar();
        return $estado;
       
//        return $this->hasOne(Modulosmatricula::className(), ['id_modulo' => $modulo])
//                    ->from(Modulosmatricula::tableName())
//                    ->viaTable(matriculas::tableName(), ['id' => 'id_matricula'], 
//                        function($query) {
//                          $query->onCondition(['dni_alumno ='.$alumno]);
//                      })
//                      ->select('estado')
//                      ->max(id);
//                      
        
        }

//        return $this->hasOne(Modulosmatricula::className(), ['id_modulo' => $modulo])->viaTable('matriculas',['id' => 'id_matricula'],
//                        function($query) {
//                                return $query->andWhere(['dni_alumno' =>$alumno]);
//                        }
//                        );
}
