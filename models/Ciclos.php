<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciclos".
 *
 * @property int $id
 * @property string|null $referencia
 * @property string|null $denominacion
 * @property string|null $reflegal1
 * @property string|null $reflegal2
 *
 * @property Matriculas[] $matriculas
 * @property Modulosciclo[] $modulosciclos
 */
class Ciclos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciclos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referencia'], 'string', 'max' => 50],
            [['denominacion', 'reflegal1', 'reflegal2'], 'string', 'max' => 200],
            [['referencia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referencia' => 'Referencia',
            'denominacion' => 'Denominacion',
            'reflegal1' => 'Reflegal1',
            'reflegal2' => 'Reflegal2',
        ];
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matriculas::className(), ['id_ciclo' => 'id']);
    }

    /**
     * Gets query for [[Modulosciclos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulosciclos()
    {
        return $this->hasMany(Modulosciclo::className(), ['id_ciclo' => 'id']);
    }
}
