<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modulos".
 *
 * @property int $id
 * @property string|null $nombre
 *
 * @property Modulosciclo[] $modulosciclos
 */
class Modulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Modulosciclos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulosciclos()
    {
        return $this->hasMany(Modulosciclo::className(), ['id_modulo' => 'id']);
    }
    
      public function getModulosciclos0()
    {
        return $this->hasMany(Modulosciclo::className(), ['id_modulo' => 'id']);
    }
}
