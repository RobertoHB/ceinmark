<?php

namespace app\models;

use Yii;
use yii\data\SqlDataProvider;
use app\models\Ciclos;
use app\models\Modulos;

/**
 * This is the model class for table "modulosciclo".
 *
 * @property int $id
 * @property int|null $id_ciclo
 * @property int|null $id_modulo
 * @property int|null $curso
 *
 * @property Ciclos $ciclo
 * @property Modulos $modulo
 * @property Modulosmatricula[] $modulosmatriculas
 */
class Modulosciclo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modulosciclo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ciclo', 'id_modulo', 'curso'], 'integer'],
            [['id_ciclo'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclos::className(), 'targetAttribute' => ['id_ciclo' => 'id']],
            [['id_modulo'], 'exist', 'skipOnError' => true, 'targetClass' => Modulos::className(), 'targetAttribute' => ['id_modulo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_ciclo' => 'Id Ciclo',
            'id_modulo' => 'Id Modulo',
            'curso' => 'Curso',
        ];
    }

    /**
     * Gets query for [[Ciclo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCiclo()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }
     public function getNombreCiclo($ciclo)
    {
        $modelCiclos = new Ciclos(); 
        return $modelCiclos::find()->select('referencia')
                ->where(['id'=> $ciclo])
                ->scalar();
    }
    public function getDenomCiclo($ciclo)
    {
        $modelCiclos = new Ciclos(); 
        return $modelCiclos::find()->select('denominacion')
                ->where(['id'=> $ciclo])
                ->scalar();
    }
     public function getCiclo0()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }

    
    /**
     * Gets query for [[Modulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulos::className(), ['id' => 'id_modulo']);
    }
    
       public function getNombreModulo($modulo)
    {
           $modelModulo = new Modulos();
         return $modelModulo::find()
                 ->select('nombre')
                ->where(['id'=> $modulo])
                ->scalar();
    }
    
     public function getModulo0()
    {
        return $this->hasOne(Modulos::className(), ['id' => 'id_modulo']);
    }
    /**
     * Gets query for [[Modulosmatriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    
    
    
    public function getModulosmatriculas()
    {
        return $this->hasMany(Modulosmatricula::className(), ['id_modulo' => 'id']);
    }
    
    
    //creados para que coincidan las tablas con las relaciones en informe dinámico
     public function getModulos()
    {
        return $this->hasOne(Modulos::className(), ['id' => 'id_modulo']);
    }
    
     public function getCiclos()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }
    
   //-----------------------------------------------------------------------------
    
    
    public function getAlumnosAQ($ciclo,$modulo,$curso){
        
        $ano_actual = date('y'); 
        $curso_acad = $ano_actual.'-'.($ano_actual + 1);
        
        return (Alumnos::find()->innerJoin('matriculas', 'matriculas.dni_alumno = alumnos.dni')
                ->select(['dni','apellidos','nombre'])
                ->andWhere(['matriculas.id_ciclo' => $ciclo])
                ->andWhere(['matriculas.curso_academico' => $curso_acad])
                ->andWhere(['matriculas.curso' => $curso])
                ->innerJoin('modulosmatricula','modulosmatricula.id_matricula = matriculas.id')              
                ->andWhere(['modulosmatricula.id_modulo'=>$modulo])
                ->innerJoin('modulos', 'modulosmatricula.id_modulo = modulos.id')
               // ->andWhere(['modulos.id'=>'modulosmatricula.id_modulo'])
   
                ->orderBy('alumnos.apellidos ASC')
                );
    }
     public function getAlumnosSQLDP($ciclo,$modulo,$curso){
        $ano_actual = date('y'); 
        $curso_acad = $ano_actual.'-'.($ano_actual + 1);
        return $datos = new SqlDataProvider([
         'sql' => "SELECT a.dni dni,a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci,a.domicilio domicilio,a.cp cp,
                     a.localidad localidad, a.tel_fijo fijo, a.movil movil,a.email email      
                          FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                                            JOIN modulosmatricula modmat ON modmat.id_matricula = m.id
                                            JOIN modulos mo on modmat.id_modulo = mo.id

                                            WHERE m.id_ciclo = $ciclo
                                            and m.curso_academico = '$curso_acad'
                                                and m.curso = $curso
                                                    and modmat.id_modulo = $modulo",   
         ]); 
     }
}
