<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Matriculas;

/**
 * MatriculasSearch represents the model behind the search form of `app\models\Matriculas`.
 */
class MatriculasSearch extends Matriculas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_ciclo', 'id_datos_bancarios', 'seguro'], 'integer'],
            [['dni_alumno', 'fecha', 'tipo', 'curso_academico'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Matriculas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_ciclo' => $this->id_ciclo,
            'id_datos_bancarios' => $this->id_datos_bancarios,
            'fecha' => $this->fecha,
            'seguro' => $this->seguro,
        ]);

        $query->andFilterWhere(['like', 'dni_alumno', $this->dni_alumno])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'curso_academico', $this->curso_academico]);

        return $dataProvider;
    }
}
