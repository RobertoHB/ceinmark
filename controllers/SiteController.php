<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\FormRegister;
use app\models\Users;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\db;
use app\models\Alumnos;
use app\models\Modulosciclo;
use \Mpdf\Mpdf;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\grid\GridView;
use yii\widgets\ListView;




class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
          
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    
    
    //confirmacion login y registro
    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
  
    public function actionConfirm()
    {
    $table = new Users;
    if (Yii::$app->request->get())
    {
   
        //Obtenemos el valor de los parámetros get
        $id = Html::encode($_GET["id"]);
        $authKey = $_GET["authKey"];
    
        if ((int) $id)
        {
            //Realizamos la consulta para obtener el registro
            $model = $table
            ->find()
            ->where("id=:id", [":id" => $id])
            ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
 
            //Si el registro existe
            if ($model->count() == 1)
            {
                $activar = Users::findOne($id);
                $activar->activate = 1;
                if ($activar->update())
                {
                    echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                    echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                }
                else
                {
                    echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                    echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                }
             }
            else //Si no existe redireccionamos a login
            {
                return $this->redirect(["site/login"]);
            }
        }
        else //Si id no es un número entero redireccionamos a login
        {
            return $this->redirect(["site/login"]);
        }
    }
 }
 

    /**
     * Displays about page.
     *
     * @return string
     */
 public function actionRegister()
 {
  //Creamos la instancia con el model de validación
  $model = new FormRegister;
   
  //Mostrará un mensaje en la vista cuando el usuario se haya registrado
  $msg = null;
   
  //Validación mediante ajax
//  if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
//        {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return ActiveForm::validate($model);
//        }
   
  //Validación cuando el formulario es enviado vía post
  //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
  //También previene por si el usuario tiene desactivado javascript y la
  //validación mediante ajax no puede ser llevada a cabo
  if ($model->load(Yii::$app->request->post()))
  {
      
   if($model->validate())
   {
    //Preparamos la consulta para guardar el usuario
    $table = new Users;
    $table->username = $model->username;
    $table->email = $model->email;
//    $table->tipo = "invitado";
    //Encriptamos el password
    $table->password = crypt($model->password, Yii::$app->params["salt"]);
    //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
    //clave será utilizada para activar el usuario
    $table->authKey = $this->randKey("abcdef0123456789", 200);
    //Creamos un token de acceso único para el usuario
    $table->accessToken = $this->randKey("abcdef0123456789", 200);
     
    //Si el registro es guardado correctamente
    if ($table->insert())
    {
     //Nueva consulta para obtener el id del usuario
     //Para confirmar al usuario se requiere su id y su authKey
     $user = $table->find()->where(["email" => $model->email])->one();
     $id = urlencode($user->id);
     $authKey = urlencode($user->authKey);
      
     $subject = "Confirmar registro";
     $body = "<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>";
     $body .= "<a href='http://127.0.0.1/.../index.php?r=site/confirm&id=".$id."&authKey=".$authKey."'>Confirmar</a>";
      
     //Enviamos el correo
     Yii::$app->mailer->compose()
     ->setTo($user->email)
     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
     ->setSubject($subject)
     ->setHtmlBody($body)
     ->send();
     
     $model->username = null;
     $model->email = null;
     $model->password = null;
     $model->password_repeat = null;
     
     $msg = "Enhorabuena, ahora sólo falta que confirmes tu registro en tu cuenta de correo";
    }
    else
    {
     $msg = "Ha ocurrido un error al llevar a cabo tu registro";
    }
     
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("register", ["model" => $model, "msg" => $msg]);
 }
 

 
 //funcion que convierte en objeto un modelo pasado como argumento
// protected function findModel($id,$models)
//    {
//        $modelx=Yii::createObject([
//          'class' => "app\models\\".$models,
//         ]);
//        if (($model = $modelx::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('The requested page does not exist.');
//    }
    
    //-----------------------------------
    
 
    public function actionGenerarpdfs($matricula,$tipo){
         return $this->redirect(array_merge(['pdfmatricula','matricula'=>$matricula,'tipo'=>$tipo]));
        
    }
    
     public function actionImpresosblanco(){
        $logo =  '../web/img/logo.png';
        $logo_gob =  '../web/img/gobcantabria.jpg';
        $portada = '<body>
<main>

<div class="todo">
<div class="primera">
    <div class="formapago">
        <h4>4.- FORMA DE PAGO</h4>

        <div class="conborde">
            <div>
                <span>Banco: <input type="text" size="80" value=""/></span>
            </div>

            <div>
                <span>Número de Cuenta: <input type="text" size="80" value=""/></span>
            </div>

            <div>
                <span>Nombre del Titular: <input type="text" size="80" value=""/></span>
            </div>
        </div>
    </div>
    <div class="finaldoc">
        <p style="padding:0 8px; text-align: justify;">Afirmo, bajo mi responsabilidad, que los datos del presente impreso son ciertos y acepto las condiciones establecidas.</p>
        <p style="text-align: center;">En Santander, a <input type="text" size="55" value="" /></p>
        <p style="text-align: center; padding-top: 50px;">Firma del Alumno/a</p>
    </div>
</div>   
<div class="segunda col-6">
    <table style="padding-top:100px">
        <tr>
            <th colspan="4" style="padding-left:275px;padding-bottom:40px">
                <div class="cabecera" >
                    <img src="'.$logo.'" width="240px"/>
                </div>
            </th>
        </tr>
        <tr>
            <td colspan="2" rowspan="2"  align="center">
                <div class="foto" style=""></div>
            </td>
            <td colspan="2" rowspan="2">
                <div class="titulo" style="">
                    <h4>AÑO ACADÉMICO: <input type="text" size="12"  value=""/></h4>
                    <h4 style="margin-top: -30px;">TURNO: <input type="text" size="6" /></h4>
                    <h4 style="margin-top: -20px;">Ciclo en que se matricula: <input type="text" size="6" value=""></h4>
                </div>
            </td>
        </tr>
    </table>

    <div class="datospersonales" style="padding-top:40px">
        <h4>1.- DATOS PERSONALES</h4>
        <div class="conborde">
            <div>
               	<span>Apellidos: <input type="text" size="113" value=""/></span>
            </div>

            <div>
		<span>Nombre: <input type="text" size="50" value=""/></span>
		<span>Fecha Nacimiento: <input type="text" size="31" value=""/></span>
            </div>
			
            <div>
		<span>Localidad Nacimiento: <input type="text" size="24" value=""/></span>
		<span>Provincia Nacimiento: <input type="text" size="30" value=""/></span>
            </div>

            <div>
		<span>DNI: <input type="text" size="30" value=""/></span>
            </div>
        </div>
    </div>

    <div class="datosfamiliares">
	<h4>2.- DATOS FAMILIARES</h4>
			
        <div class="conborde">
    
            <div>
                <span>Nombre Tutor/a : <input type="text" size="104" value=""/></span>
            </div>
            
            <div>
                <span>Nombre Padre : <input type="text" size="104" value=""/></span>
            </div>
             <div>
                <span>Nombre Madre : <input type="text" size="104" value=""/></span>
            </div>
            <div>
                <span>Domicilio: <input type="text" size="112" value=""/></span>
            </div>

            <div>
                <span>CP: <input type="text" maxlength="5" size="30" value=""/></span>
                <span>Localidad: <input type="text" size="71" value=""/></span>
            </div>

            <div>
                <span>Teléfono Móvil: <input type="text" maxlength="9" size="38" value=""/></span>
                <span>Teléfono Fijo: <input type="text" maxlength="9" size="39" value=""/></span>
            </div>

            <div>
               <span>Email: <input type="text" size="117" value=""/></span>
            </div>
        </div>
    </div>	

    <div class="datosacademicos">
	<h4>3.- DATOS ACADÉMICOS</h4>
		
	<div class="conborde">
            <div>
                <span>Centro Anterior: <input type="text" size="103" value=""/></span>
            </div>
				
            <div>
                <span>Estudios Realizados: <input type="text" size="96" value=""/></span>
            </div>

            <div>
                <span>Observaciones: <br><textarea rows="6" cols="120"></textarea></span>
            </div>
	</div>
    </div>
</div>
</div>
</body>';
                     
      
        $matricula_pg1='<body>
 <main>
    <table  style="font-size: 9pt; border-collapse: collapse;" cellpadding="8">

        <tr>
        <td>
                <img src="'.$logo_gob.'" width="120px" style="padding-bottom: 7px">
                <br><h6>CONSEJERÍA DE EDUCACIÓN,<br> 
                CULTURA Y DEPORTE</h6>
        </td>

        <td width="400"></td>
    
        <td>
                <img src="'.$logo.'" width="170px"/><br>
                <h6>C/ Vargas, 65<br>
                39010 SANTANDER<br>
                Tf: 942 23 13 44</h6>
        </td>
        

        </tr>

    </table>
   
      
<table>  
    <tr>
        <td colspan="2" align= "center" style= "font-size: 12px;width:500px;height:120px;">
            <h1>SOLICITUD DE MATRÍCULA</h1>
            <h2>CURSO 20/21</h2>
        </td>
        
        <td style="border: 1px solid black;">
            <input type ="text" size="24" value="Nº Expediente" />
        </td>    
        <td style="border:1px solid black;width:5px;">
           
               
            
         </td>

    </tr>
</table>
<table style="font-size: 45px; border-collapse: collapse;" cellpadding="10">

    <tr>       
        <td colspan="4"><span style="font-size:50px">1.- DATOS DEL ALUMNO</span></td>
    </tr>
                   

    <tr>
        <td>
            <p>DNI</p>
        </td> 
         <td colspan="2">
            <p>Apellidos</p>
        </td> 
         <td>
            <p>Nombre</p>
        </td> 

    </tr>   
    <tr>
        <td><input type="text" class="datos_alumno" maxlength="9" size="40" value=""/></td>
        <td colspan="2"><input type="text" class="datos_alumno" size="102" value="" /></td>
        <td colspan="2"><input type="text" class="datos_alumno" size="78" value="" /></td>
    </tr>

    <tr>
        <td><p>Teléfono Móvil<br><input type="text" class="datos_alumno" maxlength="9" size="40" value=""/></p></td>
        <td><p>Teléfono Fijo<br><input type="text" class="datos_alumno" maxlength="9" size="50" value=""/></p></td>
        <td colspan="3" ><p>Email<br><input type="text" class="datos_alumno" size="77" value=""/></p></td>
    </tr>

    <tr>
        <td><p>Fecha Nacimiento<br><input type="text" class="datos_alumno" size="40" value=""/></p></td>
        <td colspan=""><p>Localidad Nacimiento<br><input type="text" class="datos_alumno" size="50" value=""/></p></td>
        <td colspan="2"><p>Provincia Nacimiento<br><input type="text" class="datos_alumno" size="50" value=""/></p></td>
    </tr>

    <tr>
        <td colspan="2"><p>Domicilio<br><input type="text" class="datos_alumno" size="92" value=""/></p></td>
        <td><p>Localidad<br><input type="text" class="datos_alumno" size="50" value=""/></p></td>
        <td><p>CP<br><input type="text" maxlength="5" class="datos_alumno" size="25" value=""/></p></td>
        <td><p>Provincia<br><input type="text" class="datos_alumno" size="50" value=""/></p></td>
    </tr>

</table>
    <table style="font-size: 15pt; border-collapse: collapse;" cellpadding="8">


        <tr>
            <td>
                <span>2.- DATOS DE LOS PADRES</span>
            </td>
        </tr>

               

        <tr>
            <td>
                <span>
                    3.- DATOS ACADEMICOS
                </span>
            </td>
        </tr>
        <tr>
                <td colspan="1"><p>Código<br><input type="text" size="30" value="" /></p></td>
                <td colspan="1"><p>Ciclo en que se matricula<br><input type="text" size="70" value="" /></p></td>
                <td colspan="2"><p>Titulación con la que accede a este curso (alumnos nuevos)<br><input type="text" size="60" value="" /></p></td>
        </tr>

        <tr>
                <td><p>Curso en que se matricula<br><input class="paddleft" type="text" maxlength="1" size="50" value="" /></p></td>
                <td><p class="repite">¿Repite curso?<input class="check" type="checkbox"/></p></td>
                <td><p>Centro Anterior:<br><input type="text" size="60" value="" /></p></td>
        </tr>
        </table>
        
        <table>

        <tr>
                <td colspan="2"><p>En el caso de que no se matricule de un curso completo, relacione los módulos de los que se matricula:</p></td>
        </tr>
        <tr>

        
            
          
            <tr>      
                <td><p><span class="numeros">1.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">3.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">5.- </span><input type="text" size="38" /></p></td>
            </tr>

            <tr>
                <td><p><span class="numeros">2.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">4.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">6.- </span><input type="text" size="38" /></p></td>
            </tr></table>
                
        <table>   
        

        <tr>
                <td colspan="2"><p>Módulos pendientes del curso anterior:</p></td>
        </tr><tr>
                    
            
                                 <td colspan="1"><span class="numeros"> - </span><input type="text" size="70" value="" /></td>
                                </tr><tr>
                    
                                 <td colspan="1"><span class="numeros"> - </span><input type="text" size="80" value="" /></td>
                               
             
                        <tr>
                            <td><p><span class="numeros">1.- </span><input type="text" size="62" /></p></td>
                            <td><p><span class="numeros">2.- </span><input type="text" size="62" /></p></td>
                        </tr>        
                  
                    


    </table>
    <table style="font-size: 15pt; border-collapse: collapse;" cellpadding="8">
        <tr>
            <td colspan="2">
                <span>
                    4.- DATOS ADMINISTRATIVOS
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2"><p>¿Es la 1ª vez que se matricula en el centro?<input class="check" type="checkbox" /></p></td>
            <td colspan="2"><p>¿Abono Seguro Escolar?<input class="check" type="checkbox" /></p></td>
        </tr>

        <tr>
            <td colspan="3"><p>Estudios en que estuvo matriculado el curso anterior:</p></td>
        </tr>

        <tr>
            <td><p><span>Año</span><br><input type="text" size="40" /></p></td>
            <td colspan="2"><p><span>Curso</span><br><input type="text" size="55" /></p></td>
            <td><p><span>Centro</span><br><input type="text" size="57" /></p></td>
        </tr>

        <tr>
            <td colspan="4"><p>Quedo enterado de que la Inscripción a la que se refiere esta solicitud, está condicionada a la comprobación de los datos que en ella constan, de cuya veracidad me hago responsable.</p></td>
        </tr>

        <tr>
            <td colspan="2" style="height:80px;text-align:left;top:0;padding:0"><p style="border: 0"><span>Santander, a </span><input class="fecha" style="font-weight: bold" type="text" size="60" value=""></p>
            <td colspan="2" rowspan="2" style=""><span style="border:1;display:block"><p>Sello del Centro</p></span></td>
            <td></td>
           
        </tr>
    

        <tr>
            <td style="width:400px;text-align:center;height:150px;">
               <p></p>
               <p>Firma</p>
            </td>
            
             <td>
              
            </td>
             <td colspan="2" >
              
            </td>
        </tr>
        
       
        

        <tr>
            <td colspan="4"><p>Los datos que figuran en este impreso son objeto de protección y quedarán amparados por el secreto estadístico (Art. 13.1 de la LFEP)</p></td>
        </tr>
    </table>
</main>
</body>';        
                
                
        $matricula_pg2 = '<div class="doc1"><div class="contenido"><p>El interesado, a través de la suscripción del presente documento presta su consentimiento para que sus datos personales facilitados voluntariamente'
                        . ' sean tratados, por INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L como responsable del tratamiento, con la finalidad de poder utilizar sus datos para '
                        . 'informarle a usted de aspectos relacionados con la formación que cursa y otras formaciones que pudieran ser de su interés, y conservados durante el periodo '
                        . 'estipulado por la Consejería de Educación, Formación Profesional y Turismo del Gobierno de Cantabria. Los datos recabados del interesado podrán ser '
                        . 'comunicados a terceras entidades para el cumplimiento de las obligaciones legales. Del mismo modo declara haber sido informado sobre la posibilidad de '
                        . 'ejercitar los derechos de acceso, rectificación o supresión de sus datos, dirigiéndose a CALLE VARGAS 65 ENTLO (ACCESO C/PUENTE VIESGO) 39010 '
                        . 'SANTANDER CANTABRIA, asì mismo para obtener información adicional al respecto, podrá consultar la Política de Privacidad en www.ceinmark.net.</p><br><br>'
                        . '<p>Fdo.</p><br>'
                        . '<p>Nombre y apellidos</p><br></div></div>';           
                     
     
     
        $autorizacion_cabe='<img src="'.$logo.'" width="20%"/><br>'
                                .'<h3 class="titul_azul">DATOS DEL ALUMNO</h3><table class="table">'
                                .'<tr class="titulo"><td>Nombre</td><td>Apellidos</td><td>Fecha nacimiento</td></tr>'
                                . '<tr class="datos"><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td></tr>'
                                . '<tr class="titulo"><td>Domicilio</td><td>Codigo postal</td><td>Localidad</td></tr>'
                                . '<tr class="datos"><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td></tr>'
                                . '<tr class="titulo"><td>Telefono domicilio</td><td>Telefono Movil</td><td>Email</td></tr>'
                                . '<tr class="datos"><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td></tr>'
                                . '<tr class="titulo"><td>Curso Académico</td><td>Ciclo Formativo</td><td>Curso</td></tr>'
                                . '<tr class="datos"><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td><td><input type="text" size="65"/></td></tr>'
                        . '</table>';
               
              
        $autorización_clausula='        
                         <h4>Cláusula consentimiento expreso(art.  LOPD)</h4><div class="doc1"><p>De conformidad con lo dispuesto por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de carácter personal, consiento'
                        .'que mis datos sean incorporados a n fichero responsabilidad de INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L. y que sean tratados con la finalidad de'
                        .'INDICAR FINALIDAD. Así mismo, declaro haber sido informado sobre la posibilidad de ejercitar los derechos de acceso, rectificación, cancelación y oposición'
                        .'sobre mis datos, mediante escrito, acompañado de copia del documento oficial que acredite mi identidad, dirigido a INSTITUTO DE FORMACION Y CAPACITACION'
                        .'CEINMARK S.L. A través de correo electrónico en la dirección ceinmark@ceinmark.net, indicando enla línea de Asunto el derecho que deseo ejercitar, o mediante'
                        .'correo ordinario remitido a CALLE VARGAS Nº 65 ENTLO(ACCESO C/PUENTE VIESGO), 39010 SANTANDER, CANTABRIA.</p><br><br>'
                        .'<pre>En Santander, a  '.'<span><b></b></span></pre><br>'
                        .'<pre>Fdo.</pre><br>'
                        .'<pre>Fdo. Tutor.-</pre>';
        
        $autorizacion_autorizo_sn = '
          
                        <pre><b>               Autorizo            No Autorizo</b></pre>
                            
   
                        <br><pre>A representarme ante el citado centro educativo y actuar como interlocutores con el mismo.</pre>
                                        <br><pre>En Santander, a  <span><b></b></span></pre>
                                        <br><pre>Fdo.</pre>
                                        <pre></pre>
                                        <pre>Fdo.-                               Fdo.-</pre>';
                
        $datosbanco= '<body>
	<main>
            <div class="container">
                <div class="row">
                        <div class="col-12 logo">
                                <img src="../web/img/logo.png"/>
                        </div>
                </div>
                <div class="row">
                    <div class="col-12">
                            <h2>Datos de Domiciliación Bancaria</h2>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>DNI</h5>
                        <p></p>
                    </div>
                    <div class="col-12">
                        <h5>APELLIDOS</h5>
                        <p></p>
                    </div>
                    <div class="col-12">
                        <h5>NOMBRE</h5>
                        <p></p>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>ENTIDAD BANCARIA</h5>
                         <p></p>
                    </div>
                    <div class="col-12">
                        <h5>NÚMERO DE CUENTA</h5>
                         <p></p>
                    </div>
                    <div class="col-12">
                        <h5>NOMBRE DEL TITULAR</h5>
                        <p></p>
                    </div>
                     <div class="col-12">
                        <h5>DNI DEL TITULAR</h5>
                        <p></p>
                    </div>
                </div>
		</div>
                
            </div>
            <div class="row" style="margin-top:25px;text-align:right;padding-right:70px;">
                <div class="col-12">
                    <h5>FIRMA</h5>
                </div>
            </div>
	</main>
    </body>';
        $recibo='<body>
            <main>
                <div class="container">
                    <div class="row">
                            <div class="col-12 logo">
                                    <img src="../web/img/logo.png"/>
                            </div>
                    </div>

                    <div class="row">
                            <div class="col-12">
                                    <h2>Recibo</h2>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                            <div class="col-12">
                                    <h5>He recibido de:</h5>
                                    <p></p>
                            </div>
                            <div class="col-12">
                                    <h5>Importe:</h5>
                                    <p></p>
                            </div>
                            <div class="col-12">
                                    <h5>En concepto de:</h5>
                                    <p><p/>
                            </div>
                    </div>

                    <div class="row">
                            <div class="col-12 pie">
                                  <p class="fecha">Santander a, </p>   
                            </div>
                    </div>
                </div>

            </main>
        </body>'; 
        $carpeta='<main><div class="cabecera">   
                                <img src="'.$logo.'" width="320px"/>
                                <h3 style="font-weight:normal">CURSO ACADÉMICO: </h3>
                            </div>
               
              
                <div class="ciclo">
			<h3 style="font-weight:normal">CICLO FORMATIVO DE:</h3> 
				<br><h3></h3>
		</div>

		<div class="curso">
			<h3>CURSO:</h3>
		</div>

		<div class="alumno">
			<h3>NOMBRE Y APELLIDOS:</h3> 
                        <h3></h3>
                        <h3></h3>
		</div></main>';
        
        
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8','orientation' => '',
            "format" => "A3-L",
            "margin_bottom" => 3,
            "margin_top" => 3,
//          "margin_footer" => 0
        ]);
        
        $css_portada = file_get_contents('../web/pdf/css/portada.css');
        $css_carpeta = file_get_contents('../web/pdf/css/carpeta.css');
        $css_matricula = file_get_contents('../web/pdf/css/matricula.css');
        $css_autorizacion = file_get_contents('../web/pdf/css/autorizacion.css');
        $css_recibo = file_get_contents('../web/pdf/css/recibo.css');
        
       
        $mpdf->WriteHTML($css_portada, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($portada, \Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->AddPage();
        $mpdf->WriteHTML($css_carpeta, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($carpeta, \Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        $mpdf->WriteHTML($css_matricula, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($matricula_pg1, \Mpdf\HTMLParserMode::HTML_BODY);  
        $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        //$mpdf->WriteHTML($css_matricula, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($matricula_pg2, \Mpdf\HTMLParserMode::HTML_BODY);  
         $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        $mpdf->WriteHTML($css_autorizacion, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($autorizacion_cabe.$autorización_clausula, \Mpdf\HTMLParserMode::HTML_BODY);   
        $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        $mpdf->WriteHTML($css_autorizacion, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($autorizacion_cabe.$autorizacion_autorizo_sn, \Mpdf\HTMLParserMode::HTML_BODY);  
        
        $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        $mpdf->WriteHTML($css_recibo, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($recibo, \Mpdf\HTMLParserMode::HTML_BODY);  
        
        $mpdf->AddPageByArray(array(
            'orientation' => 'P',
            'sheet-size' => 'A4-P',
        ));
        $mpdf->WriteHTML($css_recibo, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($datosbanco, \Mpdf\HTMLParserMode::HTML_BODY); 
        
        $mpdf->Output();
        
        
        
        
        
     }
    
    
    
    
    
    
    
    public function actionPdfrecibo($alumno,$fechamat){
      
        $fecha = $this->txt_fecha($fechamat);
                      
                 
        $datos = new SqlDataProvider([
                'sql' => "SELECT nombre, apellidos 
                                 FROM alumnos WHERE dni = '$alumno'",   
           ]); 
         
  
            $resultado = $datos->getModels();
            
            $nombre =  $resultado[0]['nombre'];
            $apellidos =  $resultado[0]['apellidos'];
        
        
        $contenido = '
        <body>
	<main>
            <div class="container">
                <div class="row">
                        <div class="col-12 logo">
                                <img src="../web/img/logo.png"/>
                        </div>
                </div>

                <div class="row">
                        <div class="col-12">
                                <h2>Recibo</h2>
                        </div>
                </div>

                <hr>

                <div class="row">
                        <div class="col-12">
                                <h5>He recibido de:</h5>
                                <p>'.$nombre." ".$apellidos.'</p>
                        </div>
                        <div class="col-12">
                                <h5>Importe:</h5>
                                <p>1,12 €</p>
                        </div>
                        <div class="col-12">
                                <h5>En concepto de:</h5>
                                <p>Seguro Escolar<p/>
                        </div>
                </div>

                <div class="row">
                        <div class="col-12 pie">
                              <p class="fecha">Santander a, '.$fecha.'</p>   
                        </div>
                </div>
            </div>

	</main>
    </body>';
                
          $mpdf = NEW Mpdf([
               
           ]);
        $css = file_get_contents('../web/pdf/css/recibo.css');
      
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);

        $mpdf->Output();
    }
     
    public function actionPdfdatosbanco($alumno,$codigobanco){
      
////        $fecha_matricula = getdate();
//        $fecha = $this->txt_fecha($fecha_matricula);
        $datos = new SqlDataProvider([
                'sql' => "SELECT nombre, apellidos, dni, passnie 
                                 FROM alumnos WHERE dni = '$alumno'",   
           ]); 
         
  
            $resultado = $datos->getModels();
            $passnie = $resultado[0]['passnie'];
            $dni = $resultado[0]['dni'];
            $nombre =  $resultado[0]['nombre'];
            $apellidos =  $resultado[0]['apellidos'];
        
            
        $datosBanco =  new SqlDataProvider([
                'sql' => "SELECT banco,iban,titular,dni 
                                 FROM datosbancarios WHERE id = $codigobanco",   
           ]);   
        
         $resultado_bancos = $datosBanco->getModels();
            $entidad = $resultado_bancos[0]['banco'];
            $iban =  $resultado_bancos[0]['iban'];
            $titular =  $resultado_bancos[0]['titular'];
            $dni_titular =  $resultado_bancos[0]['dni'];
            
        $contenido = '
       
        <body>
	<main>
            <div class="container">
                <div class="row">
                        <div class="col-12 logo">
                                <img src="../web/img/logo.png"/>
                        </div>
                </div>
                <div class="row">
                    <div class="col-12">
                            <h2>Datos de Domiciliación Bancaria</h2>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>DNI</h5>
                        <p>'.$passnie.$dni.'</p>
                    </div>
                    <div class="col-12">
                        <h5>APELLIDOS</h5>
                        <p>'.$apellidos.'</p>
                    </div>
                    <div class="col-12">
                        <h5>NOMBRE</h5>
                        <p>'.$nombre.'</p>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>ENTIDAD BANCARIA</h5>
                         <p>'.$entidad.'</p>
                    </div>
                    <div class="col-12">
                        <h5>NÚMERO DE CUENTA</h5>
                         <p>'. substr($iban, 0,4)." - ".substr($iban, 4,4)." - ".substr($iban, 8,4)." - ".substr($iban, 12,2)." - ".substr($iban, 14,10).'</p>
                    </div>
                    <div class="col-12">
                        <h5>NOMBRE DEL TITULAR</h5>
                        <p>'.$titular.'</p>
                    </div>
                     <div class="col-12">
                        <h5>DNI DEL TITULAR</h5>
                        <p>'.$dni_titular.'</p>
                    </div>
                </div>
		</div>
                
            </div>
            <div class="row" style="margin-top:25px;text-align:right;padding-right:70px;">
                <div class="col-12">
                    <h5>FIRMA</h5>
                </div>
            </div>
	</main>
    </body>';
                
          $mpdf = NEW Mpdf([
               
           ]);
        $css = file_get_contents('../web/pdf/css/recibo.css');
      
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);

        $mpdf->Output();
    }
    

        public function actionPdfmatricula($matricula,$tipo){
      
        if(isset($matricula)){
            $datos = new SqlDataProvider([
                'sql' => "SELECT a.dni dni,a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci, 
                                 a.loc_nac localnaci, a.prov_nac provnaci, a.domicilio domicilio,a.cp cp,
                                 a.localidad localidad,a.provincia provincia, a.tel_fijo fijo, a.movil movil,a.email email,
                                 a.centro_ant ca, a.tit_acceso titacc,a.passnie passnie,
                                 c.referencia referencia,c.denominacion denominacion,
                                 m.curso curso,m.fecha,m.curso_academico cursoacademico, m.repite repite,m.privez primeravez,m.seguro seguro, m.id_ciclo ciclo
                                 FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                                                   JOIN ciclos c ON m.id_ciclo = c.id 
                                                   WHERE m.id = $matricula",   
           ]); 
         
  
            $resultado = $datos->getModels();
            
            $dni_alumno =  $resultado[0]['dni'];
            $nombre = $resultado[0]['nombre'];
            $apellidos = $resultado[0]['apellidos'];
            $movil =  $resultado[0]['movil'];
            $fijo = $resultado[0]['fijo'];
            $email =  $resultado[0]['email'];
            $fnaci =  $resultado[0]['fechanaci'];
            $locnaci =  $resultado[0]['localnaci'];
            $provnaci =  $resultado[0]['provnaci'];
            $domicilio = $resultado[0]['domicilio'];
            $localidad =  $resultado[0]['localidad'];
            $cp = $resultado[0]['cp'];
            $provincia = $resultado[0]['provincia'];
            $ciclo = $resultado[0]['ciclo'];
            $referCiclo = $resultado[0]['referencia'];
            $nombreCiclo =  $resultado[0]['denominacion'];
            $curso = $resultado[0]['curso']; 
            
            $titulacion_acceso = $resultado[0]['titacc'];
            $centro_anterior =  $resultado[0]['ca'];
            
             $pass_nie =  $resultado[0]['passnie'];
            
            $fecha_matricula =  $resultado[0]['fecha'];
            
           
           
            $input_checked = '<input class="check" type="checkbox" checked="checked" />';
            $input_unchecked = '<input class="check" type="checkbox" />';
            
            $repite = $resultado[0]['repite'];
            ($repite === '1' )? $repite = " checked = \"checked\"" : $repite = "";
         
           
            $privez = $resultado[0]['primeravez'];
//            ($privez === '1' )? $privez = " checked = \"checked\"" : $privez = "";
            
            if ($privez === '1'){
                $privez = " checked = \"checked\"";
                $titulacion_acceso = $resultado[0]['titacc'];
                $centro_anterior =  $resultado[0]['ca'];
            }else{
                $privez = "";
                $titulacion_acceso = "";
                $centro_anterior =  "";
            }
            
            $seguro = $resultado[0]['seguro'];
            ($seguro === '1' )? $seguro = " checked = \"checked\"" : $seguro = "";

            
            //registros de tutores o responsables
            
             $datos_tutores = new SqlDataProvider([
                'sql' => "SELECT t.nombre nombreTutor,t.apellidos apeltutor,t.dni dni,t.telefono telef FROM alumnos a JOIN responsables r ON
                     a.dni = r.dni_alumno JOIN
                        tutores t ON t.id = r.id_tutor WHERE a.dni = '$dni_alumno'",   
           ]); 
            $resultado_tutores = $datos_tutores->getModels();
          
            
            
            // compara modulos/ciclo con modulos matricula
             $datos_compara_modulos = new SqlDataProvider([
                'sql' => "SELECT c1.C1 - c2.C2 COMPLETO FROM(
                        (SELECT COUNT(*) C1 FROM modulosciclo m WHERE m.id_ciclo = '$ciclo' and m.curso = '$curso') c1,
                        (SELECT COUNT(*) C2 FROM modulosmatricula mm WHERE mm.id_matricula = '$matricula') c2
                          )",   
           ]); 
            $resultado_compara_modulos_ARR =  $datos_compara_modulos->getModels();
            $resultado_compara_modulos = $resultado_compara_modulos_ARR[0]['COMPLETO'];
            
        // $datos_compara_modulos->getCount();
            
            // registros de modulos en los que se matricula
             $datos_modulos_matricula = new SqlDataProvider([
                'sql' => "select modulos.nombre nombre FROM matriculas m JOIN"
                            . " modulosmatricula m1 ON m1.id_matricula = m.id JOIN"
                       . " modulos ON modulos.id = m1.id_modulo WHERE "
                       . "m.id = '$matricula' 
                           ",   
           ]); 
            $resultado_modulos_matricula = $datos_modulos_matricula->getModels();
         
            
            // modulos pendientes curso anterior
              //Primero comprobamos que se ha matriculado mas de una vez en este ciclo
            $countMatriculas_en_Ciclo = Yii::$app->db->createCommand('SELECT count(*) FROM '
                                . 'matriculas mat join'
                                . ' modulosmatricula modmat on mat.id = modmat.id_matricula WHERE'
                                . ' mat.dni_alumno = "20195088F" AND mat.id_ciclo ='. $ciclo.' and mat.curso = 1'
                                . ' AND mat.id < '. $matricula.''
                                . ' AND modmat.estado = "NA"')->queryScalar();
           
            if($countMatriculas_en_Ciclo >0){
                $datos_modulos_pendientes = new SqlDataProvider([
                    'sql' => "SELECT modu.nombre nombre FROM matriculas mat JOIN"
                    . " modulosmatricula modmat on mat.id = modmat.id_matricula JOIN"
                    . " modulos modu on modu.id = modmat.id_modulo WHERE"
                    . " mat.dni_alumno = '$dni_alumno' AND"
                    . " mat.id_ciclo = $ciclo AND mat.curso = 1 AND"
                    . " modmat.estado = 'NA' AND mat.id < $matricula",   
                ]);   
                  $resultado_modulos_ptes_ARR =  $datos_modulos_pendientes->getModels();
            }

            
            
            
            
            
            
            
            
            $ruta_firma = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_firma_exi = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_pdf = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/';
            $logo =  '../web/img/logo.png';
            $logo_gob =  '../web/img/gobcantabria.jpg';
            $foto_alumno = '../web/img/alumnos/'.$dni_alumno.'/personal/foto.png';
            
            
            $fecha = $this->txt_fecha($fecha_matricula);

            if (!file_exists($ruta_firma_exi)){
                $imagenFirma = "";
            }else{
                $imagenFirma = '<img src="'.$ruta_firma.'" width="20%" />';
            }
            if (!file_exists($foto_alumno)){
                $imagenFoto = '<input class="sinborde" type="text" name="" value="Foto">';
            }else{
                $imagenFoto = '<img src="'.$foto_alumno.'" width="15%" class="sinborde" style="" />';
            }
            
        }
            
        $contenido = '
<body>
 <main>
    <table  style="font-size: 9pt; border-collapse: collapse;" cellpadding="8">

        <tr>
        <td>
                <img src="'.$logo_gob.'" width="120px" style="padding-bottom: 7px">
                <br><h6>CONSEJERÍA DE EDUCACIÓN,<br> 
                CULTURA Y DEPORTE</h6>
        </td>

        <td width="400"></td>
    
        <td>
                <img src="'.$logo.'" width="170px"/><br>
                <h6>C/ Vargas, 65<br>
                39010 SANTANDER<br>
                Tf: 942 23 13 44</h6>
        </td>
        

        </tr>

    </table>
   
      
<table>  
    <tr>
        <td colspan="2" align= "center" style= "font-size: 12px;width:500px;height:120px;">
            <h1>SOLICITUD DE MATRÍCULA</h1>
            <h2>CURSO 20/21</h2>
        </td>
        
        <td style="border: 1px solid black;">
            <input type ="text" size="24" value="Nº Expediente" />
        </td>    
        <td style="border:1px solid black;width:5px;">
           
                '.$imagenFoto.'
            
         </td>

    </tr>
</table>
<table style="font-size: 45px; border-collapse: collapse;" cellpadding="10">

    <tr>       
        <td colspan="4"><span style="font-size:50px">1.- DATOS DEL ALUMNO</span></td>
    </tr>
                   

    <tr>
        <td>
            <p>DNI</p>
        </td> 
         <td colspan="2">
            <p>Apellidos</p>
        </td> 
         <td>
            <p>Nombre</p>
        </td> 

    </tr>   
    <tr>
        <td><input type="text" class="datos_alumno" maxlength="9" size="40" value="'.$pass_nie.$dni_alumno.'"/></td>
        <td colspan="2"><input type="text" class="datos_alumno" size="102" value="'.$apellidos.'" /></td>
        <td colspan="2"><input type="text" class="datos_alumno" size="78" value="'.$nombre.'" /></td>
    </tr>

    <tr>
        <td><p>Teléfono Móvil<br><input type="text" class="datos_alumno" maxlength="9" size="40" value="'.$movil.'"/></p></td>
        <td><p>Teléfono Fijo<br><input type="text" class="datos_alumno" maxlength="9" size="50" value="'.$fijo.'"/></p></td>
        <td colspan="3" ><p>Email<br><input type="text" class="datos_alumno" size="77" value="'.$email.'"/></p></td>
    </tr>

    <tr>
        <td><p>Fecha Nacimiento<br><input type="text" class="datos_alumno" size="40" value="'.$fnaci.'"/></p></td>
        <td colspan=""><p>Localidad Nacimiento<br><input type="text" class="datos_alumno" size="50" value="'.$locnaci.'"/></p></td>
        <td colspan="2"><p>Provincia Nacimiento<br><input type="text" class="datos_alumno" size="50" value="'.$provnaci.'"/></p></td>
    </tr>

    <tr>
        <td colspan="2"><p>Domicilio<br><input type="text" class="datos_alumno" size="92" value="'.$domicilio.'"/></p></td>
        <td><p>Localidad<br><input type="text" class="datos_alumno" size="50" value="'.$localidad.'"/></p></td>
        <td><p>CP<br><input type="text" maxlength="5" class="datos_alumno" size="25" value="'.$cp.'"/></p></td>
        <td><p>Provincia<br><input type="text" class="datos_alumno" size="50" value="'.$provincia.'"/></p></td>
    </tr>

</table>
    <table style="font-size: 15pt; border-collapse: collapse;" cellpadding="8">


            <tr>
            <td>
            <span>2.- DATOS DE LOS PADRES</span></td></tr>';

                foreach ($resultado_tutores as $valor) {
                    $contenido .= ' <tr>   
                                       <td><p>Nombre y Apellidos del Padre o Tutor<br><input type="text" size="72" value="'.$valor["nombreTutor"]." ".$valor["apeltutor"].'" /></p></td>
                                       <td><p>DNI<br><input type="text" maxlength="9" size="45" value="'.$valor["dni"].'" /></p></td>
                                       <td><p>Teléfono<br><input type="text" maxlength="9" size="45" value="'.$valor["telef"].'" /></p></td>
                                    </tr>';
                 }

            $contenido .= '   

        <tr>
            <td>
                <span>
                    3.- DATOS ACADEMICOS
                </span>
            </td>
        </tr>
        <tr>
                <td colspan="1"><p>Código<br><input type="text" size="30" value="'.$referCiclo.'" /></p></td>
                <td colspan="1"><p>Ciclo en que se matricula<br><input type="text" size="70" value="'.$nombreCiclo.'" /></p></td>
                <td colspan="2"><p>Titulación con la que accede a este curso (alumnos nuevos)<br><input type="text" size="60" value="'.$titulacion_acceso.'" /></p></td>
        </tr>

        <tr>
                <td><p>Curso en que se matricula<br><input class="paddleft" type="text" maxlength="1" size="50" value="'.$curso.'" /></p></td>
                <td><p class="repite">¿Repite curso?<input class="check" type="checkbox"'.$repite.' /></p></td>
                <td><p>Centro Anterior:<br><input type="text" size="60" value="'.$centro_anterior.'" /></p></td>
        </tr>
        </table>
        
        <table>

        <tr>
                <td colspan="2"><p>En el caso de que no se matricule de un curso completo, relacione los módulos de los que se matricula:</p></td>
        </tr>
        <tr>';

        
            
            if($resultado_compara_modulos <> 0){
                $contador = 1;
                foreach ($resultado_modulos_matricula as $valor) {
                   
                    if ($contador%2==0 ){
                       $contenido .='<td>'.$contador.'.- <input type="text" size="70" value="'.$valor["nombre"].'"/></td></tr><tr>';     
                     }else{
                         $contenido .= '<td>'.$contador.'.- <input type="text" size="80" value="'.$valor["nombre"].'"/></td>';
                    }
                    $contador ++;
                 }
            }else{
                $contenido .= '
            <tr>      
                <td><p><span class="numeros">1.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">3.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">5.- </span><input type="text" size="38" /></p></td>
            </tr>

            <tr>
                <td><p><span class="numeros">2.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">4.- </span><input type="text" size="37" /></p></td>
                <td><p><span class="numeros">6.- </span><input type="text" size="38" /></p></td>
            </tr>';        
                        
                        
            }
            
            $contenido .= '</table>
                
        <table>   
        

        <tr>
                <td colspan="2"><p>Módulos pendientes del curso anterior:</p></td>
        </tr><tr>';
                    
            if($countMatriculas_en_Ciclo >0 ){    
                $contador = 1;
                foreach ($resultado_modulos_ptes_ARR as $valor) {
                     if ($contador%2==0 ){
                        $contenido .= '
                                 <td colspan="1"><span class="numeros">'.$contador.'.- </span><input type="text" size="70" value="'.$valor["nombre"].'" /></td>>
                                </tr><tr>';
                     }else{
                        $contenido .= '
                                 <td colspan="1"><span class="numeros">'.$contador.'.- </span><input type="text" size="80" value="'.$valor["nombre"].'" /></td>>
                                ';   
                     }   
                    $contador ++;
                }
            }else{
                 $contenido .= '
                        <tr>
                            <td><p><span class="numeros">1.- </span><input type="text" size="62" /></p></td>
                            <td><p><span class="numeros">2.- </span><input type="text" size="62" /></p></td>
                        </tr>';        
            }
                        
                        
             $contenido .= '          
                    


    </table>
    <table style="font-size: 15pt; border-collapse: collapse;" cellpadding="8">
        <tr>
            <td colspan="2">
                <span>
                    4.- DATOS ADMINISTRATIVOS
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2"><p>¿Es la 1ª vez que se matricula en el centro?<input class="check" type="checkbox"'.$privez.' /></p></td>
            <td colspan="2"><p>¿Abono Seguro Escolar?<input class="check" type="checkbox"'.$seguro.' /></p></td>
        </tr>

        <tr>
            <td colspan="3"><p>Estudios en que estuvo matriculado el curso anterior:</p></td>
        </tr>

        <tr>
            <td><p><span>Año</span><br><input type="text" size="40" /></p></td>
            <td colspan="2"><p><span>Curso</span><br><input type="text" size="55" /></p></td>
            <td><p><span>Centro</span><br><input type="text" size="57" /></p></td>
        </tr>

        <tr>
            <td colspan="4"><p>Quedo enterado de que la Inscripción a la que se refiere esta solicitud, está condicionada a la comprobación de los datos que en ella constan, de cuya veracidad me hago responsable.</p></td>
        </tr>

        <tr>
            <td colspan="2" style="height:80px;text-align:left;top:0;padding:0"><p style="border: 0"><span>Santander, a </span><input class="fecha" style="font-weight: bold" type="text" size="60" value="'.$fecha.'"></p>
            <td colspan="2" rowspan="2" style=""><span style="border:1;display:block"><p>Sello del Centro</p></span></td>
            <td></td>
           
        </tr>
    

        <tr>
            <td style="width:400px;text-align:center;height:150px;">
               <p>'.$imagenFirma.'</p>
               <p>Firma</p>
            </td>
            
             <td>
              
            </td>
             <td colspan="2" >
              
            </td>
        </tr>
        
       
        

        <tr>
            <td colspan="4"><p>Los datos que figuran en este impreso son objeto de protección y quedarán amparados por el secreto estadístico (Art. 13.1 de la LFEP)</p></td>
        </tr>
    </table>
</main>
</body>';        
                
                
        $contenido_pag2 = '<div class="doc1"><div class="contenido"><p>El interesado, a través de la suscripción del presente documento presta su consentimiento para que sus datos personales facilitados voluntariamente'
                        . ' sean tratados, por INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L como responsable del tratamiento, con la finalidad de poder utilizar sus datos para '
                        . 'informarle a usted de aspectos relacionados con la formación que cursa y otras formaciones que pudieran ser de su interés, y conservados durante el periodo '
                        . 'estipulado por la Consejería de Educación, Formación Profesional y Turismo del Gobierno de Cantabria. Los datos recabados del interesado podrán ser '
                        . 'comunicados a terceras entidades para el cumplimiento de las obligaciones legales. Del mismo modo declara haber sido informado sobre la posibilidad de '
                        . 'ejercitar los derechos de acceso, rectificación o supresión de sus datos, dirigiéndose a CALLE VARGAS 65 ENTLO (ACCESO C/PUENTE VIESGO) 39010 '
                        . 'SANTANDER CANTABRIA, asì mismo para obtener información adicional al respecto, podrá consultar la Política de Privacidad en www.ceinmark.net.</p><br><br>'
                        . '<p>Fdo.</p>'.$imagenFirma.'<br>'
                        . '<p>Nombre y apellidos</p><br>'.$nombre.' '.$apellidos.'</div></div>';           
                
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8','orientation' => '',
            "format" => "A4",
    //           
                "margin_bottom" => 3,
                "margin_top" => 3,
    //            "margin_footer" => 0
        ]);
        
        $css = file_get_contents('../web/pdf/css/matricula.css');
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->AddPage();
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido_pag2,\Mpdf\HTMLParserMode::HTML_BODY); 
         if ($tipo == 'V'){
                    $mpdf->Output(); 
                }else{
                     if(file_exists($ruta_pdf.'matricula.pdf')){
                        $nombre_fichero = $ruta_pdf.'matricula.pdf';
                        unlink($nombre_fichero);
                    }    
                    $mpdf->Output($ruta_pdf.'matricula.pdf',\Mpdf\Output\Destination::FILE);
                     return $this->redirect(array_merge(['pdfautorizacion','matricula'=>$matricula,'tipo'=>$tipo]));
                    

                }
                return $this->redirect(Yii::$app->request->referrer);
    }
    
    
    
     public function actionPdfautorizacion($matricula,$tipo){
   
  
        if(isset($matricula)){
            $datos = new SqlDataProvider([
                   'sql' => "SELECT a.dni dni,a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci,a.domicilio domicilio,a.cp cp,
                               a.localidad localidad, a.tel_fijo fijo, a.movil movil,a.email email,
                               c.denominacion denominacion,m.curso curso,m.curso_academico cursoacademico,
                               m.fecha fecha_matricula
                                    FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                                                      JOIN ciclos c ON m.id_ciclo = c.id 
                                                      WHERE m.id = $matricula",   
            ]); 
         
  
            $resultado = $datos->getModels();
            
            $dni_alumno =  $resultado[0]['dni'];
            $fecha_matricula =  $resultado[0]['fecha_matricula'];
            $array_cursos = ["1"=>"Primero","2"=>"Segundo"];
            $fecha= $this->txt_fecha($fecha_matricula);
            
            $datos_tutores = new SqlDataProvider([
                'sql' => " SELECT t.nombre nombreTutor,t.apellidos apeltutor,t.dni dni,t.telefono telef FROM alumnos a JOIN responsables r ON
                     a.dni = r.dni_alumno JOIN
                        tutores t ON t.id = r.id_tutor WHERE a.dni = '$dni_alumno'",   
            ]); 
            $resultado_tutores = $datos_tutores->getModels();
            
            $ruta_firma = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_firma_exi = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_pdf = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/';
            $logo =  '../web/img/logo.png';
           
            if (!file_exists($ruta_firma_exi)){
                $imagenFirma = "";
            }else{
                $imagenFirma = '<img src="'.$ruta_firma.'" width="20%" />';
            }
    }
       
        
           $mpdf = NEW Mpdf([
               
           ]);

                
                $cabecera1='<img src="'.$logo.'" width="20%"/><br>'
                                .'<h3 class="titul_azul">DATOS DEL ALUMNO</h3><table class="table">'
                                .'<tr class="titulo"><td>Nombre</td><td>Apellidos</td><td>Fecha nacimiento</td></tr>'
                                . '<tr class="datos"><td>'.$resultado[0]['nombre'].'</td><td>'.$resultado[0]['apellidos'].'</td><td>'.$resultado[0]['fechanaci'].'</td></tr>'
                                . '<tr class="titulo"><td>Domicilio</td><td>Codigo postal</td><td>Localidad</td></tr>'
                                . '<tr class="datos"><td>'.$resultado[0]['domicilio'].'</td><td>'.$resultado[0]['cp'].'</td><td>'.$resultado[0]['localidad'].'</td></tr>'
                                . '<tr class="titulo"><td>Telefono domicilio</td><td>Telefono Movil</td><td>Email</td></tr>'
                                . '<tr class="datos"><td>'.$resultado[0]['fijo'].'</td><td>'.$resultado[0]['movil'].'</td><td>'.$resultado[0]['email'].'</td></tr>'
                                . '<tr class="titulo"><td>Curso Académico</td><td>Ciclo Formativo</td><td>Curso</td></tr>'
                                . '<tr class="datos"><td>'.$resultado[0]['cursoacademico'].'</td><td>'.$resultado[0]['denominacion'].'</td><td>'.$array_cursos[$resultado[0]['curso']].'</td></tr>'
                        . '</table>';
               
              
                
                $cuerpo1 = '<h4>Cláusula consentimiento expreso(art.  LOPD)</h4><div class="doc1"><p>De conformidad con lo dispuesto por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de carácter personal, consiento'
                        .'que mis datos sean incorporados a n fichero responsabilidad de INSTITUTO DE FORMACION Y CAPACITACION CEINMARK S.L. y que sean tratados con la finalidad de'
                        .'INDICAR FINALIDAD. Así mismo, declaro haber sido informado sobre la posibilidad de ejercitar los derechos de acceso, rectificación, cancelación y oposición'
                        .'sobre mis datos, mediante escrito, acompañado de copia del documento oficial que acredite mi identidad, dirigido a INSTITUTO DE FORMACION Y CAPACITACION'
                        .'CEINMARK S.L. A través de correo electrónico en la dirección ceinmark@ceinmark.net, indicando enla línea de Asunto el derecho que deseo ejercitar, o mediante'
                        .'correo ordinario remitido a CALLE VARGAS Nº 65 ENTLO(ACCESO C/PUENTE VIESGO), 39010 SANTANDER, CANTABRIA.</p><br><br>'
                        .'<pre>En Santander, a  '.'<span><b>'.$fecha.'</b></span></pre><br>'
                        .'<pre>Fdo.</pre>'.$imagenFirma.'<br>'
                        .'<pre>Fdo. Tutor.-</pre>';

          
                $cuerpo2 = '<pre><b>               Autorizo            No Autorizo</b></pre>';
                            $c = 0;
                            foreach ($resultado_tutores as $valor) {
                               $c = $c + 1;
                               $texto_tutor = "A ";
                               if ($c == 2) $texto_tutor = "Y/o a: ";
                               $cuerpo2 .=  '<div style="width:2000px"><pre>'.$texto_tutor." ".'<span><b>'.$valor["nombreTutor"]." ".$valor["apeltutor"].'</b></span>           Con D.N.I. nº <b>'.$valor["dni"].'</b></pre></div>
                                            <div style="width:2000px"><pre>padre/madre/tutor legal, con nº de telefono   <span><b>'.$valor["telef"].'</b></span></pre>';
                                                             
                            }
   
                            $cuerpo2.= '<br><pre>A representarme ante el citado centro educativo y actuar como interlocutores con el mismo.</pre>
                                        <br><pre>En Santander, a  <span><b>'.$fecha.'</b></span></pre>
                                        <br><pre>Fdo.</pre>
                                        <pre>'.$imagenFirma.'</pre>
                                        <pre>Fdo.-                               Fdo.-</pre>';
                
                $css = file_get_contents('../web/pdf/css/autorizacion.css');
                $contenido1 = $cabecera1.$cuerpo1;
                $contenido2 = $cabecera1.$cuerpo2;
                
                $mpdf->keep_table_proportions = true;
                $mpdf->shrink_tables_to_fit = 1;
                $mpdf->use_kwt = true; 
                $mpdf->WriteHTML($css,\Mpdf\HTMLParserMode::HEADER_CSS); 
                $mpdf->AddPage();
                $mpdf->WriteHTML($contenido1,\Mpdf\HTMLParserMode::HTML_BODY);   
                $mpdf->AddPage();
                $mpdf->WriteHTML($contenido2,\Mpdf\HTMLParserMode::HTML_BODY);
                if ($tipo == 'V'){
                    $mpdf->Output(); 
                }else{
                     if(file_exists($ruta_pdf.'autorizacion.pdf')){
                        $nombre_fichero = $ruta_pdf.'autorizacion.pdf';
                        unlink($nombre_fichero);
                    }    
                    $mpdf->Output($ruta_pdf.'autorizacion.pdf',\Mpdf\Output\Destination::FILE);
                     return $this->redirect(['/matriculas/update', 'id' => $matricula,'alumno'=>$dni_alumno]);

                }
                return $this->redirect(Yii::$app->request->referrer);
     }
    
    public function actionPdfcarpeta($matricula,$tipo){
   
  
        if(isset($matricula)){
            $datos = new SqlDataProvider([
                   'sql' => "SELECT a.nombre nombre,a.apellidos apellidos,
                              c.denominacion denominacion,m.curso curso,m.curso_academico cursoacademico,
                              m.fecha fecha_matricula
                                    FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                                                      JOIN ciclos c ON m.id_ciclo = c.id 
                                                      WHERE m.id = $matricula",   
            ]); 
         
  
            $resultado = $datos->getModels();
            $apellidos =  $resultado[0]['apellidos'];
            $nombre =  $resultado[0]['nombre'];
            $curso_academico =  $resultado[0]['cursoacademico'];
            $curso = $resultado[0]['curso'];
            $denominacion =  $resultado[0]['denominacion'];
            
            $fecha_matricula =  $resultado[0]['fecha_matricula'];
            
            $array_cursos = ["1"=>"Primero","2"=>"Segundo"];
            $fecha= $this->txt_fecha($fecha_matricula);
            
            
           
            $logo =  '../web/img/logo.png';
           
         
    }
       
     
           $mpdf = NEW Mpdf(['mode' => 'utf-8', 'format' => 'A3-L']);      
         

                
                $cabecera='<main><div class="cabecera">   
                                <img src="'.$logo.'" width="320px"/>
                                <h3 style="font-weight:normal">CURSO ACADÉMICO: '.$curso_academico.'</h3>
                            </div>';
               
              
                
                $cuerpo = '<div class="ciclo">
			<h3 style="font-weight:normal">CICLO FORMATIVO DE:</h3> 
				<br><h3>'.$denominacion.'</h3>
		</div>

		<div class="curso">
			<h3>CURSO: '.$curso.'</h3>
		</div>

		<div class="alumno">
			<h3>NOMBRE Y APELLIDOS:</h3> 
                        <h3>'.$apellidos.'</h3>
                        <h3>'.$nombre.'</h3>
		</div></main>';

          
               
                                   
                
                $css = file_get_contents('../web/pdf/css/carpeta.css');
                
                $contenido = $cabecera.$cuerpo;
               
                
//                $mpdf->keep_table_proportions = true;
//                $mpdf->shrink_tables_to_fit = 1;
//                $mpdf->use_kwt = true; 
                
                $mpdf->WriteHTML($css,\Mpdf\HTMLParserMode::HEADER_CSS); 
                $mpdf->AddPage();
                $mpdf->WriteHTML($contenido,\Mpdf\HTMLParserMode::HTML_BODY);   
               
                if ($tipo == 'V'){
                    $mpdf->Output(); 
                }
                return $this->redirect(Yii::$app->request->referrer);
     }
    
    public function actionPdfportada($matricula,$tipo){
      
        if(isset($matricula)){
            $datos = new SqlDataProvider([
                'sql' => "SELECT a.dni dni,a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci, 
                                 a.loc_nac localnaci, a.prov_nac provnaci, a.domicilio domicilio,a.cp cp,
                                 a.localidad localidad,a.provincia provincia, a.tel_fijo fijo, a.movil movil,a.email email,
                                 a.centro_ant ca, a.tit_acceso titacc,a.passnie passnie,
                                 c.referencia referencia,c.denominacion denominacion,
                                 m.fecha fecha_matricula,m.id_datos_bancarios idbanco,m.curso curso,m.curso_academico cursoacademico, m.repite repite,m.privez primeravez,m.seguro seguro
                                 FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                                                   JOIN ciclos c ON m.id_ciclo = c.id 
                                                   WHERE m.id = $matricula",   
           ]); 
         
  
            $resultado = $datos->getModels();
            
            $dni_alumno =  $resultado[0]['dni'];
            $nombre = $resultado[0]['nombre'];
            $apellidos = $resultado[0]['apellidos'];
            $movil =  $resultado[0]['movil'];
            $fijo = $resultado[0]['fijo'];
            $email =  $resultado[0]['email'];
            $fnaci =  $resultado[0]['fechanaci'];
            $locnaci =  $resultado[0]['localnaci'];
            $provnaci =  $resultado[0]['provnaci'];
            $domicilio = $resultado[0]['domicilio'];
            $localidad =  $resultado[0]['localidad'];
            $cp = $resultado[0]['cp'];
            $provincia = $resultado[0]['provincia'];
            $referCiclo = $resultado[0]['referencia'];
            $nombreCiclo =  $resultado[0]['denominacion'];
            $curso = $resultado[0]['curso']; 
            $curso_academico = $resultado[0]['cursoacademico']; 
            
            $idbanco = $resultado[0]['idbanco']; 
            
            $titulacion_acceso = $resultado[0]['titacc'];
            $centro_anterior =  $resultado[0]['ca'];
            $pass_nie =  $resultado[0]['passnie'];
            $fecha_matricula =  $resultado[0]['fecha_matricula'];
            
            $privez = $resultado[0]['primeravez'];       
            
            if ($privez === '1'){
                $titulacion_acceso = $resultado[0]['titacc'];
                $centro_anterior =  $resultado[0]['ca'];
            }else{
                $privez = "";
                $titulacion_acceso = "";
                $centro_anterior =  "";
            }
            
            $seguro = $resultado[0]['seguro'];
           

            
            //registros de tutores o responsables
            
             $datos_tutores = new SqlDataProvider([
                'sql' => " SELECT t.nombre nombreTutor,t.apellidos apeltutor,t.dni dni,t.telefono telef FROM alumnos a JOIN responsables r ON
                     a.dni = r.dni_alumno JOIN
                        tutores t ON t.id = r.id_tutor WHERE a.dni = '$dni_alumno'",   
           ]); 
            $resultado_tutores = $datos_tutores->getModels();
            
          
            //registros banco
             $datosBanco =  new SqlDataProvider([
                'sql' => "SELECT banco,iban,titular,dni 
                           FROM datosbancarios WHERE id = '$idbanco'",   
             ]);   
        
            $resultado_bancos = $datosBanco->getModels();
            
            $entidad = $resultado_bancos[0]['banco'];
            $iban =  $resultado_bancos[0]['iban'];
            $titular =  $resultado_bancos[0]['titular'];
            $dni_titular =  $resultado_bancos[0]['dni'];  
            
            
            
            $ruta_firma = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_firma_exi = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/firma.png';
            $ruta_pdf = '../web/img/alumnos/'.$resultado[0]['dni'].'/'.$matricula.'('.$resultado[0]['cursoacademico'].')/';
            $logo =  '../web/img/logo.png';
            //$logo_gob =  url::to('@web/img/gobcantabria.jpg');
            $foto_alumno = '../web/img/alumnos/'.$dni_alumno.'/personal/foto.png';
            
            $fecha = $this->txt_fecha($fecha_matricula);

            if (!file_exists($ruta_firma_exi)){
                $imagenFirma = "";
            }else{
                $imagenFirma = '<img src="'.$ruta_firma.'" width="20%" />';
            }
            if (!file_exists($foto_alumno)){
                $imagenFoto = '<input class="sinborde" type="text" name="" value="Foto">';
            }else{
                $imagenFoto = '<img src="'.$foto_alumno.'" width="30%" class="sinborde" style="" />';
            }
            
        }
            
        
   
   
$contenido = '
       
<body>
<main>

<div class="todo">
<div class="primera">
    <div class="formapago">
        <h4>4.- FORMA DE PAGO</h4>

        <div class="conborde">
            <div>
                <span>Banco: <input type="text" size="80" value="'.$entidad.'"/></span>
            </div>

            <div>
                <span>Número de Cuenta: <input type="text" size="80" value="'.$iban.'"/></span>
            </div>

            <div>
                <span>Nombre del Titular: <input type="text" size="80" value="'.$titular.'"/></span>
            </div>
        </div>
    </div>
    <div class="finaldoc">
        <p style="padding:0 8px; text-align: justify;">Afirmo, bajo mi responsabilidad, que los datos del presente impreso son ciertos y acepto las condiciones establecidas.</p>
        <p style="text-align: center;">En Santander, a <input type="text" size="55" value="'.$fecha.'" /></p>
        <p style="text-align: center; padding-top: 50px;">Firma del Alumno/a</p>
    </div>
</div>   
<div class="segunda col-6">
    <table style="padding-top:100px">
        <tr>
            <th colspan="4" style="padding-left:275px;padding-bottom:40px">
                <div class="cabecera" >
                    <img src="'.$logo.'" width="240px"/>
                </div>
            </th>
        </tr>
        <tr>
            <td colspan="2" rowspan="2"  align="center">
                <div class="foto" style="">'.$imagenFoto.'</div>
            </td>
            <td colspan="2" rowspan="2">
                <div class="titulo" style="">
                    <h4>AÑO ACADÉMICO: <input type="text" size="12"  value="'.$curso_academico.'"/></h4>
                    <h4 style="margin-top: -30px;">TURNO: <input type="text" size="6" /></h4>
                    <h4 style="margin-top: -20px;">Ciclo en que se matricula: <input type="text" size="6" value="'.$referCiclo.'"></h4>
                </div>
            </td>
        </tr>
    </table>

    <div class="datospersonales" style="padding-top:40px">
        <h4>1.- DATOS PERSONALES</h4>
        <div class="conborde">
            <div>
               	<span>Apellidos: <input type="text" size="113" value="'.$apellidos.'"/></span>
            </div>

            <div>
		<span>Nombre: <input type="text" size="50" value="'.$nombre.'"/></span>
		<span>Fecha Nacimiento: <input type="text" size="31" value="'.$fnaci.'"/></span>
            </div>
			
            <div>
		<span>Localidad Nacimiento: <input type="text" size="24" value="'.$locnaci.'"/></span>
		<span>Provincia Nacimiento: <input type="text" size="30" value="'.$provnaci.'"/></span>
            </div>

            <div>
		<span>DNI: <input type="text" size="30" value="'.$pass_nie.$dni_alumno.'"/></span>
            </div>
        </div>
    </div>

    <div class="datosfamiliares">
	<h4>2.- DATOS FAMILIARES</h4>
			
        <div class="conborde">';
    if (!empty($resultado_tutores)) {
        foreach ($resultado_tutores as $valor) {
            
            $contenido .= '
            <div>
                <span>Nombre Tutor/a : <input type="text" size="104" value="'.$valor['nombreTutor'].' '.$valor['apeltutor'].'"/></span>
            </div>';
            
            }
            
    }else{
            $contenido .= '
            <div>
                <span>Nombre Padre : <input type="text" size="104" value=""/></span>
            </div>
             <div>
                <span>Nombre Madre : <input type="text" size="104" value=""/></span>
            </div>';
    }     
          $contenido .= '

            <div>
                <span>Domicilio: <input type="text" size="112" value="'.$domicilio.'"/></span>
            </div>

            <div>
                <span>CP: <input type="text" maxlength="5" size="30" value="'.$cp.'"/></span>
                <span>Localidad: <input type="text" size="71" value="'.$localidad.'"/></span>
            </div>

            <div>
                <span>Teléfono Móvil: <input type="text" maxlength="9" size="38" value="'.$movil.'"/></span>
                <span>Teléfono Fijo: <input type="text" maxlength="9" size="39" value="'.$fijo.'"/></span>
            </div>

            <div>
               <span>Email: <input type="text" size="117" value="'.$email.'"/></span>
            </div>
        </div>
    </div>	

    <div class="datosacademicos">
	<h4>3.- DATOS ACADÉMICOS</h4>
		
	<div class="conborde">
            <div>
                <span>Centro Anterior: <input type="text" size="103" value="'.$centro_anterior.'"/></span>
            </div>
				
            <div>
                <span>Estudios Realizados: <input type="text" size="96" value="'.$titulacion_acceso.'"/></span>
            </div>

            <div>
                <span>Observaciones: <br><textarea rows="6" cols="120"></textarea></span>
            </div>
	</div>
    </div>
</div>
</div>
</body>';
                          
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8','orientation' => '',
            "format" => "A3-L",
    //           
                "margin_bottom" => 3,
                "margin_top" => 3,
    //            "margin_footer" => 0
        ]);
        
        $css = file_get_contents('../web/pdf/css/portada.css');
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);
//        $mpdf->AddPage();
//        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
//        $mpdf->WriteHTML($contenido_pag2,\Mpdf\HTMLParserMode::HTML_BODY); 
         if ($tipo == 'V'){
                    $mpdf->Output(); 
                }else{
                     if(file_exists($ruta_pdf.'matricula.pdf')){
                        $nombre_fichero = $ruta_pdf.'matricula.pdf';
                        unlink($nombre_fichero);
                    }    
                    $mpdf->Output($ruta_pdf.'matricula.pdf',\Mpdf\Output\Destination::FILE);
                     return $this->redirect(array_merge(['pdfautorizacion','matricula'=>$matricula,'tipo'=>$tipo]));
                    

                }
                return $this->redirect(Yii::$app->request->referrer);
    }
    
    
    
    
     
     
     
     
     
     
     
    
    
        public function actionGeneradorinformes(){
            return $this->render('/site/informes');

        }
        public function actionDatostablas(){
            $tablas = Yii::$app->db->schema->tableNames;
            
           
            return json_encode($tablas);
        }
        public function actionCampostablas($tabla){           
            $clase_instanciar = "app\\models\\".$tabla;
            $obj =  new $clase_instanciar;
          
            
                    
            
        
//            if($tabla === 'Alumnos'){
//                $obj = new Alumnos();
//            }
//            if($tabla === 'Bancos'){
//               $obj = new \app\models\Bancos();
//           }
//            if($tabla === 'Ciclos'){
//               $obj = new \app\models\Ciclos();
//           }
//            if($tabla === 'Datosbancarios'){
//               $obj = new \app\models\Datosbancarios();
//           }
//            if($tabla === 'Matriculas'){
//               $obj = new \app\models\Matriculas();
//           }
//           
//           if($tabla === 'Modulos'){
//               $obj = new Modulos();
//           }
//            if($tabla === 'Modulosciclo'){
//               $obj = new \app\models\Modulosciclo();
//           }
//            if($tabla === 'Modulosmatricula'){
//               $obj = new \app\models\Modulosmatricula();
//           }
//            if($tabla === 'Responsables'){
//               $obj = new \app\models\Responsables();
//           }
//            if($tabla === 'Tutores'){
//               $obj = new \app\models\Tutores();
//           }
// 
   
           
            
            $campos = array_keys($obj->attributes); 
            return json_encode($campos);
        }     
        
        
        
      
     public function actionInforme_dinamico_pantalla(){
         $datos = $_POST['datos'];
        
         
       $query_columnas =  $this->consulta_informe_dinamico($datos);
       $query = $query_columnas['query'];
       $columnas = $query_columnas['columnas'];
       
         
        $dataProvider = new ActiveDataProvider([
                  'query' => $query,
                  //'sort'=> ['defaultOrder' => ['dni'=>SORT_ASC]],
                 'pagination' => [
                      'pageSize' => 0,
                  ],
              ]);

        return(                     
            GridView::widget([
                'dataProvider' => $dataProvider,
                'summary'=>'',
                'showFooter'=>false,
                'showHeader' => true,
                'columns' => $columnas,           
            ])
        );
   
         
     }
    
    
    
     public function actionInforme_dinamico_excel(){
//         var_dump($_GET['datos']);
//         exit;
         
       $datos = json_decode($_GET['datos'],true);
       
     
       
//       echo("<pre>");
//       print_r($datos['campos']);
//       echo("</pre>");
//       exit;
       $query_columnas =  $this->consulta_informe_dinamico($datos);
       
       $query = $query_columnas['query'];
       $columnas = $query_columnas['columnas'];
       $cadena = [];
       $encabezado = [];
       foreach($columnas as $campo){
           
           $valor_campo = $campo['attribute'];
           $valor_encabezado = $campo['label'];
           $cadena[]= $valor_campo;
           $encabezado[] = $valor_encabezado;
          
       }
       
    
        //$encabezado =  rtrim($encabezado, ',');
//       echo $cadena;
//       exit;
//       var_dump($columnas);
//       exit;
      
//        
    $file = \Yii::createObject([
        'class' => 'codemix\excelexport\ExcelFile',
//
        'writerClass' => '\PHPExcel_Writer_Excel2007', // Override default of `\PHPExcel_Writer_Excel2007`
//       
//
        'sheets' => [
// 
//       
//
            'Listado' => [
                'class' => 'codemix\excelexport\ActiveExcelSheet',
                'query' => $query,
                'startRow' => 5 ,
                 'on beforeRender' =>function ( $event ) {
//                    $sheet = $event->sender->getSheet();
////                    
////                  
                },
////                     
//            
////            // If not specified, all attributes from `User::attributes()` are used
            'attributes' =>
               $cadena
            ,
                'titles' => $encabezado,
//                    
             'styles' => [
                 'A5:E5' => [
                     'font' => [
                        'bold' =>true ,
                        'color' => [ 'rgb' => '000000' ],
                        
                        'size' => 10 ,
                        'name' => 'Verdana'
                        ],
////                   
                   'fill' => [
                            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startcolor' => [
                                'argb' => 'FFDEAD',
                            ],
                            'endcolor' => [
                                'argb' => 'FFFAF0',
                            ],
                        ],
                ],
            ],
//
//            // If not specified, the label from the respective record is used.
//            // You can also override single titles, like here for the above `team.name`
////            'titles' => [
////                'A' => 'Listado de Alumnos',
//            ],
        ],

    ],
//                        
    ]);
    $file->getWorkbook()->getSheet(0)->getStyle('B1:B3')->getFont()->setSize('15')->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);            
    $file->getWorkbook()->getSheet(0)->getStyle('C1:C3')->getFont()->setSize('15')->setBold('true'); 
//    //$file->getWorkbook()->getSheet(0)->setCellValue('B1','Curso Académico ');
//   // $file->getWorkbook()->getSheet(0)->setCellValue('C1',$curso_acad);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('B2','Ciclo Formativo');
////     $file->getWorkbook()->getSheet(0)->getStyle('B2')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('C2',$nombre_ciclo);
//   // $file->getWorkbook()->getSheet(0)->setCellValue('B3','Denominación');
////     $file->getWorkbook()->getSheet(0)->getStyle('B3')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('C3',$denominacion_ciclo);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('A')->setAutoSize(true); 
    $file->getWorkbook()->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('F')->setAutoSize(true); 
    $file->getWorkbook()->getSheet(0)->getColumnDimension('G')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('H')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('I')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('J')->setAutoSize(true);
//    
    $file->send('informe.xlsx');
        
    }
    
    public function consulta_informe_dinamico($datos){
        
            
            
            if(isset($datos)){
          
                //$data = json_decode($_POST['datos'],true);
                $tablas =  $datos['tablas'];
                $campos = $datos['campos'];  
                if(isset($datos['criterios'])){
                    $criterios =  $datos['criterios'];  
                }   
                if(isset($datos['grupos'])){
                    $grupos =  $datos['grupos'];  
                }   
                if(isset($datos['orden'])){
                    $orden =  $datos['orden'];  
                }   
                
                $titulo =  $datos['titulo'][0]; 
                $salida =  $datos['salida'][0]; 
                
                
                       
//                var_dump($tablas);
//                var_dump($campos);
//                var_dump($criterios);
//                var_dump($salida);
////                
//               exit;   
                $contador = count($campos);
                
               
                
                $tabla_1 = strtolower($tablas[0]);
  
                $str_campos=[];
                $str_join="";
                $str_tabla_first = "";
                $str_tablas=[];
                $str_campos_sin_prefijo = "";
                $cadena_tablas = "";
                $tablas_orden = [];
                $tablas_find = ['Matriculas','Responsables'];
              
                $html_listado ='<table class="table-bordered table-striped mb-0">
                    <thead>
                    <tr>';
              
              
                //reaordenamos las tablas para que contemple la existencia de Matriculas y la situe en primer lugar para el find
                
                foreach($tablas as $valor_){
                    $tablas_orden[] = $valor_;
                }
                
                
                foreach ($tablas_orden as $key => $valor) {
                   // $primero = reset($tablas);
                   
                    foreach ($tablas_find as $valor1){
                        
                        if($valor == $valor1){
                            if($key <> 0){
                                $primero = $tablas_orden[0];
                                $tablas_orden[$key] = $primero;
                                $tablas_orden[0] = $valor1;
                            }
                        }
                          
                    }
                }
                
                foreach ($tablas_orden as $tbl_nombres) {
                    $tabla_minus = strtolower($tbl_nombres);
                    if ( $tbl_nombres === reset( $tablas_orden ) ) { 
                        //guardamos primera tabla del array
                        $str_tabla_first = $tbl_nombres;
                    }else{
                        //resto de tablas del array para hacer el with
                        
                        $str_tablas[] = $tabla_minus;
                    } 
                  
                }
//                
//                var_dump($str_tabla_first);
//                exit;
                
                //creamos las relaciones con with
                $tablas_relacion = "";
                foreach ($str_tablas as $tbl_rel) {
                    $tablas_relacion .= "'$tbl_rel',";
                    
                }
                 
               
                $tablas_relacion_sinlast = substr($tablas_relacion, 0, -1);
//                echo $tablas_relacion_sinlast;
//                exit;
               
                
                  foreach ($str_tablas as $tbls_sinfirst) {
                      $cadena_tablas .= "'$tbls_sinfirst',"; 
                      
                  }
             
                foreach ($campos as $field_nombres) {
                  
                   $html_listado .= '<th scope="col">'.$field_nombres.'</th>';
                   //$longitud_string = strlen($field_nombres);
                   $capturar_string_tabla = explode(".", $field_nombres);
                   $longitud_string_tabla = strlen($capturar_string_tabla[0]);
                  
                   $compruebo_tabla = $capturar_string_tabla[0];
                   
                   if ($compruebo_tabla == mb_strtolower($str_tabla_first)){
                       $recojo_campo = substr($field_nombres, $longitud_string_tabla+1);
                       $str_campos[] = $recojo_campo;
                       //$str_campos .= "'$recojo_campo',";
                   }else{
                         //$str_campos .= "'$field_nombres',";
                         $str_campos[] = $field_nombres;
                   }
                  
                  
                }
//                
//                echo $str_campos;
//                exit;
                
                foreach ($campos as $field) {
                   
                 $campo_ = strstr($field, '.');
                 $str_campos_sin_prefijo .=  $campo_ ;
                 
//                  $str_campos_sin_prefijo .=  strtok($field, " .s"); // Primer token$field_nombres.",";
//                  echo($str_campos);
//                  exit;
                  
                }
                if(isset($datos['criterios'])){
                    $criterios_ = "";
                    foreach ($criterios as $cadena_criterios) {
                       //comprobamos si en los criterios existen campos de la tabla find. contamos 11 caracteres hasta el nombre de tabla 
                        $tamano_tabla = strlen(mb_strtolower($str_tabla_first));
                        $tabla_eliminar = substr($cadena_criterios,0,$tamano_tabla);  
                        if($tabla_eliminar == mb_strtolower($str_tabla_first)){
                            $criterio_sin_prefijo = str_replace($tabla_eliminar.".", "", $cadena_criterios);
                            $criterios_ .= $criterio_sin_prefijo.",";
                        }else{
                            $criterios_ .= $cadena_criterios.",";
                        }
                    }
                     $criterios_sin_coma_final =  rtrim($criterios_, ',');
                     $array_criterios = explode(",", $criterios_sin_coma_final);
                
               } 
//                echo $criterios_;
//                exit;
//            
                
                
                
                
              
               
                //---------------------------------
                $camposs = strtr($str_campos_sin_prefijo, ".", ",");
                $camposs = substr($camposs, 1);  
                $array_campos = explode(",", $camposs);
                $cadena_tablas_ =  substr($cadena_tablas, 0, -1);
                $cadena_tablas_ = "'$cadena_tablas'";
                
           
              
                

                
                //agrupando resultados(ejemplo)
//                 $model = Category::find()
//                ->with(['feeds' => function($query) {
//                        return $query->where(['user_id' => $user_id]);
//                    }, 'feeds.feedOptions', 'feeds.feedComments', 'feeds.feedVotes'])
//                        ->asArray()
//                        ->all();
//                    return $model;
                
                
              
             
            $columnas = []; 

           foreach ($str_campos as $campos_grid) {

               $columnas[] = [
                  

                   'attribute' => $campos_grid,

                   'label'=>$campos_grid,
                 
          
               ];

           }
           
           
           
               
            $clase_tabla_principal = "app\\models\\".$str_tabla_first;
              

                
            $obj =  new $clase_tabla_principal;
                
                
//                $model_encontrar=Yii::createObject([
//                    'class' => "app\models\\".$modAlumnosels,
//                ]);
              
                
                
             
               // $criterios_sin_coma_final =  rtrim($criterios_, ',');
                 //echo $criterios_sin_coma_final;
             //exit;
                
//                $array_criterios = explode(",", $criterios_sin_coma_final);
//                var_dump($tablas_relacion_sinlast);
//                exit;

               $query=$obj::find(array(
                    'select' =>$str_campos,
                    'joinwith'=>array(
                       //$cadena_tablas
                       $tablas_relacion_sinlast
                    ),
                 
                   
                ));
               
//               $query->select(['alumnos.dni','alumnos.nombre','matriculas.id_ciclo','matriculas.tipo','matriculas.seguro']);
              
               if (isset($array_criterios)){
               
                    foreach($array_criterios as $criters){

                        $query->andWhere($criters);


                    }
               }
               
                if (isset($orden)){

                    foreach($orden as $ordenando){

                        $query->orderBy($ordenando);

                    }
                }
               
            return ["query" => $query, "columnas"=>$columnas];
               

              
   
        }
    
    }
    
    
    
    
    
    
       public function txt_fecha($fecha_mat){
           setlocale(LC_ALL,"es_ES");
          // $fecha_matricula = strtotime($fecha_mat);
          
        $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
        $meses = array("","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
        $fecha = $dias[date('N', strtotime($fecha_mat))].", ".date('d', strtotime($fecha_mat))." de ".$meses[date('n', strtotime($fecha_mat))]." de ".date("Y", strtotime($fecha_mat));
        return $fecha;

    }
    
}
