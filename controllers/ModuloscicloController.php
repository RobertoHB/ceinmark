<?php

namespace app\controllers;

use Yii;
use app\models\Modulosciclo;
use app\models\ModuloscicloSearch;
use app\models\Modulos;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use app\models\Matriculas;
use app\models\Ciclos;
use app\models\Datosbancarios;
use codemix\excelexport;
use excelexport\ExcelFile;
use Mpdf\Mpdf;
use yii\db\Query;


/**
 * ModuloscicloController implements the CRUD actions for Modulosciclo model.
 */
class ModuloscicloController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Modulosciclo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModuloscicloSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Modulosciclo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Modulosciclo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Modulosciclo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Modulosciclo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Modulosciclo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Modulosciclo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Modulosciclo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modulosciclo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     public function actionAnadirmodulo($tipo,$ciclo)
    {
//       $rows = Modulosciclo::find()
//               ->select('id','modulos.nombre nombre','curso')
//               ->join('modulos','modulos.id = id_modulo')
//               ->where(['id_ciclo' => $ciclo])
//               //->Where(['curso' => $curso])
//               ->all();
         
//         $itemModulos = ArrayHelper::map(Modulos::find()->all(), 'id', 'nombre');
        
        
             
            if($tipo === '0'){
                $cuenta = "SELECT count(*) FROM modulosciclo m JOIN modulos modu ON m.id_modulo = modu.id  WHERE m.id_ciclo = $ciclo order by m.curso";
                $consulta = "SELECT modu.id id, modu.nombre nombre,m.curso curso FROM modulosciclo m JOIN modulos modu ON m.id_modulo = modu.id  WHERE m.id_ciclo = $ciclo order by m.curso";
            }else{
                $cuenta = "SELECT count(*) FROM modulosciclo m JOIN modulos modu ON m.id_modulo = modu.id";
//                  $cuenta= "SELECT count(*) FROM modulos";
                $consulta = "SELECT DISTINCT(modu.nombre) nombre, modu.id id, m.curso curso FROM modulosciclo m JOIN modulos modu ON m.id_modulo = modu.id";
//                $consulta = "SELECT id,nombre FROM modulos";
            }
            
            $count = Yii::$app->db->createCommand($cuenta)->queryScalar();
            
            
            $dataProvider = new SqlDataProvider([
                'sql' => $consulta,    
                'totalCount' => $count,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => [
                    'attributes' => [
                    'title',
                    'view_count',
                    'created_at',
                    ],
                ],
            ]);

            $resultado = $dataProvider->getModels();
           

            echo "<option>Seleccione un modulo</option>";

            if(count($resultado)>0){
                foreach($resultado as $row){
                    $nid = $row['id'];
//                     echo "<option value='$nid'>".$row['nombre']."</option>";
                    echo "<option value='$nid'>".$row['nombre']."( C".$row['curso']." )"."</option>";
                }
            }else{
                echo "<option>Ningun modulo encontrado</option>";
            }

    }
    public function actionCirculares($ciclo,$modulo,$curso){
       
    $ano_actual = date('y'); 
    $model = new Modulosciclo();
    
    $curso_acad = $ano_actual.'-'.($ano_actual + 1);
    $nombre_ciclo = $model->getNombreCiclo($ciclo);
    $nombre_modulo = $model->getNombreModulo($modulo);

//        SELECT * FROM alumnos al JOIN matriculas m ON al.dni = m.dni_alumno JOIN ciclos c ON m.id_ciclo = c.id WHERE m.id = 118;
    $file = \Yii::createObject([
        'class' => 'codemix\excelexport\ExcelFile',

        'writerClass' => '\PHPExcel_Writer_Excel2007', // Override default of `\PHPExcel_Writer_Excel2007`
       

        'sheets' => [

            'Alumnos' => [
                'class' => 'codemix\excelexport\ActiveExcelSheet',
                'query' => Alumnos::find()->innerJoin('matriculas', 'matriculas.dni_alumno = alumnos.dni')
//                    ->innerJoin('datosbancarios','datosbancarios.id = matriculas.id_datos_bancarios')   
                    ->andWhere(['matriculas.id_ciclo' => $ciclo])
                    ->andWhere(['matriculas.curso_academico' => $curso_acad])
                    ->andWhere(['matriculas.curso' => $curso])
                    ->innerJoin('modulosmatricula','modulosmatricula.id_matricula = matriculas.id')              
                    ->andWhere(['modulosmatricula.id_modulo'=>$modulo])
                    ->innerJoin('modulos', 'modulosmatricula.id_modulo = modulos.id')
                   // ->andWhere(['modulos.id'=>'modulosmatricula.id_modulo'])

                    ->orderBy('alumnos.apellidos ASC'),
                
                'startRow' => 5 ,
                 'on beforeRender' =>function ( $event ) {
                    $sheet = $event->sender->getSheet();
                    
                  
                },
                     
            
            // If not specified, all attributes from `User::attributes()` are used
            'attributes' => [
                'dni',
                'nombre',
                'apellidos',
                'movil',
                'email',
            ],
              'titles' => ['DNI', 'NOMBRE', 'APELLIDOS','TELEFONO','EMAIL'],
                    
             'styles' => [
                 'A5:E5' => [
                     'font' => [
                        'bold' =>true ,
                        'color' => [ 'rgb' => '000000' ],
                        
                        'size' => 10 ,
                        'name' => 'Verdana'
                        ],
                   
                   'fill' => [
                            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startcolor' => [
                                'argb' => 'FFDEAD',
                            ],
                            'endcolor' => [
                                'argb' => 'FFFAF0',
                            ],
                        ],
                ],
            ],

            // If not specified, the label from the respective record is used.
            // You can also override single titles, like here for the above `team.name`
//            'titles' => [
//                'A' => 'Listado de Alumnos',
//            ],
        ],

    ],
    ]);
    $file->getWorkbook()->getSheet(0)->getStyle('B1:B3')->getFont()->setSize('15')->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);            
    $file->getWorkbook()->getSheet(0)->getStyle('C1:C3')->getFont()->setSize('15')->setBold('true'); 
    $file->getWorkbook()->getSheet(0)->setCellValue('B1','Curso Académico ');
    $file->getWorkbook()->getSheet(0)->setCellValue('C1',$curso_acad);
    $file->getWorkbook()->getSheet(0)->setCellValue('B2','Ciclo Formativo');
//     $file->getWorkbook()->getSheet(0)->getStyle('B2')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
    $file->getWorkbook()->getSheet(0)->setCellValue('C2',$nombre_ciclo);
    $file->getWorkbook()->getSheet(0)->setCellValue('B3','Módulo');
//     $file->getWorkbook()->getSheet(0)->getStyle('B3')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
    $file->getWorkbook()->getSheet(0)->setCellValue('C3',$nombre_modulo);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('A')->setAutoSize(true); 
    $file->getWorkbook()->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $file->send('alumnos.xlsx');
        
    }
    
    public function actionCuentas_bancarias($ciclo,$modulo,$curso){
    $ano_actual = date('y'); 
    $model = new Modulosciclo();
    
    $curso_acad = $ano_actual.'-'.($ano_actual + 1);
    $nombre_ciclo = $model->getNombreCiclo($ciclo);
    $nombre_modulo = $model->getNombreModulo($modulo);
    $denominacion_ciclo = $model->getDenomCiclo($ciclo);
    
//     $datos = new SqlDataProvider([
//                'sql' => "SELECT a.nombre nombre, a.apellidos apellidos, db.iban iban, db.titular titular, db.dni dnititular
//                         FROM alumnos a JOIN matriculas m on m.dni_alumno = a.dni 
//                           join datosbancarios db ON db.id = m.id_datos_bancarios 
//                           join modulosmatricula modmat on modmat.id_matricula = m.id 
//                           join modulos md on md.id = modmat.id_modulo 
//                           where (m.id_ciclo = $ciclo and m.curso_academico = '$curso_acad' and modmat.id_modulo = $modulo)",   
//           ]); 
         
  
//            $resultado = $datos->getModels();
//           $resultado_ =  ArrayHelper::toArray($resultado);
//          echo "<pre>";
//          print_r($resultado_);
//          echo "</pre>";
//           exit;
           
   
            

//        SELECT * FROM alumnos al JOIN matriculas m ON al.dni = m.dni_alumno JOIN ciclos c ON m.id_ciclo = c.id WHERE m.id = 118;
    $file = \Yii::createObject([
        'class' => 'codemix\excelexport\ExcelFile',

        'writerClass' => '\PHPExcel_Writer_Excel2007', // Override default of `\PHPExcel_Writer_Excel2007`
       

        'sheets' => [
 
       

            'Alumnos' => [
                'class' => 'codemix\excelexport\ActiveExcelSheet',
                'query' => Matriculas::find()
//                    ->distinct()

                    //->innerJoinWith('matriculas')
                    ->innerJoinWith('alumnos')
                    ->innerJoinWith('datosBancarios')
                    //->innerJoinWith('ciclo')
                   // ->leftJoin('modulos','modulos.id = modulosmatricula.id_modulo')
                    ->andWhere(['matriculas.id_ciclo' => $ciclo])
                    ->andWhere(['matriculas.curso_academico' => $curso_acad]),
                    //->andWhere(['modulosmatricula.id_modulo' => $modulo]),
                     //->select('alumnos.nombre nombre,alumnos.apellidos apellidos, datosBancarios.iban iban, datosBancarios.titular titular, datosBancarios.dni dnititular'),
                'startRow' => 5 ,
                 'on beforeRender' =>function ( $event ) {
                    $sheet = $event->sender->getSheet();
//                    
//                  
                },
//                     
            
            // If not specified, all attributes from `User::attributes()` are used
            'attributes' => [
                'alumnos.nombre',
                'alumnos.apellidos',
                'datosBancarios.iban',
                'datosBancarios.titular',
                'datosBancarios.dni',
            ],
              'titles' => ['NOMBRE', 'APELLIDOS','IBAN','TITULAR','DNI'],
                    
             'styles' => [
                 'A5:E5' => [
                     'font' => [
                        'bold' =>true ,
                        'color' => [ 'rgb' => '000000' ],
                        
                        'size' => 10 ,
                        'name' => 'Verdana'
                        ],
//                   
                   'fill' => [
                            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 90,
                            'startcolor' => [
                                'argb' => 'FFDEAD',
                            ],
                            'endcolor' => [
                                'argb' => 'FFFAF0',
                            ],
                        ],
                ],
            ],

            // If not specified, the label from the respective record is used.
            // You can also override single titles, like here for the above `team.name`
//            'titles' => [
//                'A' => 'Listado de Alumnos',
//            ],
        ],

    ],
                        
    ]);
    $file->getWorkbook()->getSheet(0)->getStyle('B1:B3')->getFont()->setSize('15')->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);            
    $file->getWorkbook()->getSheet(0)->getStyle('C1:C3')->getFont()->setSize('15')->setBold('true'); 
    $file->getWorkbook()->getSheet(0)->setCellValue('B1','Curso Académico ');
    $file->getWorkbook()->getSheet(0)->setCellValue('C1',$curso_acad);
    $file->getWorkbook()->getSheet(0)->setCellValue('B2','Ciclo Formativo');
//     $file->getWorkbook()->getSheet(0)->getStyle('B2')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
    $file->getWorkbook()->getSheet(0)->setCellValue('C2',$nombre_ciclo);
    $file->getWorkbook()->getSheet(0)->setCellValue('B3','Denominación');
//     $file->getWorkbook()->getSheet(0)->getStyle('B3')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
    $file->getWorkbook()->getSheet(0)->setCellValue('C3',$denominacion_ciclo);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('A')->setAutoSize(true); 
    $file->getWorkbook()->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $file->send('alumnos.xlsx');
        
    }
    
    public function actionAsistencia($ciclo,$modulo,$curso){
//         $datos_compara_modulos = new SqlDataProvider([
//                'sql' => "SELECT c1.C1 - c2.C2 COMPLETO FROM(
//                        (SELECT COUNT(*) C1 FROM modulosciclo m WHERE m.id_ciclo = '$ciclo' and m.curso = '$curso') c1,
//                        (SELECT COUNT(*) C2 FROM modulosmatricula mm WHERE mm.id_matricula = '$matricula') c2
//                          )",   
//           ]); 
        $alumnos_ciclo = new Modulosciclo();
        
        $ref_ciclo = $alumnos_ciclo->getNombreCiclo($ciclo);
        $nombre_modulo = $alumnos_ciclo->getNombreModulo($modulo);
       
        $alumnos = $alumnos_ciclo->getAlumnosSQLDP($ciclo, $modulo, $curso);
        
        $date = strtotime(date("d-m-y"));
        $semanaIni =  date('d-m-Y',strtotime('last monday'));
        if (date("l") === "Sunday"){
            $semanaFin =  date('d-m-Y',strtotime('last friday'));
        }else{    
            $semanaFin =  date('d-m-Y',strtotime('next friday')); 
        }
            
       
       

//        echo date('m-d-Y', $first);
//        echo '<br>';
//        echo date('m-d-Y', $last);
//        EXIT;
       
        
        $resultado =  $alumnos->getModels();
    
        
           
            
           // $resultado_compara_modulos = $resultado_compara_modulos_ARR[0]['COMPLETO'];
              $logo =  '../web/img/logo.png'; 
                $contador = 1;
           
                
           
                 $contenido = '
                        <main>
	<table class="sinbordes" style="width: 100%">
		<tr class="sinbordes">
			<th rowspan="3" style="padding-right: 40px; padding-left: 20px;" class="sinbordes"><img src="'.$logo.'" width="240px"/></th>
			<th colspan="34" class="sinbordes"><p>CENTRO DE ESTUDIOS DE FORMACIÓN PROFESIONAL</p></th>
		</tr>
		<tr class="sinbordes">
			<th colspan="34" class="sinbordes" style="padding:4px 0;border-top: 1px solid black; border-bottom: 1px solid black"><input type="text" name="curso" size="12" class="encabeza" value="'.$curso.'-'.$ref_ciclo.'" /><input type="text" name="ciclo" size="75" value="'.$nombre_modulo.'" /></th>
		</tr>
		<tr class="sinbordes">
			<th colspan="34" class="sinbordes"><p>Del <input type="data" name="fechainicio" value="'.$semanaIni.'"/> al <input type="data" name="fechafinal" value="'.$semanaFin.'"/></p></th>
		</tr>
	</table>
	<table class="cuadricula" style="width: 100%;">
		<tr>
			<th colspan="12" style="width: 28%"> &nbsp; </th>
			<th colspan="7"><span>LUNES</span></th>
			<th colspan="7"><span>MARTES</span></th>
			<th colspan="7"><span>MIÉRCOLES</span></th>
			<th colspan="7"><span>JUEVES</span></th>
			<th colspan="6"><span>VIERNES</span></th>
		</tr>
		<tr>
			<td colspan="12">Alumno</td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
		</tr>';
                foreach ($resultado as $valor) {

                  $contenido .= '
                    <tr>
			<td colspan="12" align="left">'.$contador.'.'.' '.$valor['nombre'].' '. $valor['apellidos'].' </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td style="background-color: #ccc"> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
		</tr>';  
                  $contador ++;
               }   
              
                
	$contenido .= '
	
	</table>
	</main>';        
                
                       
                
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8','orientation' => 'L',
            "format" => "A4",
    //           
                "margin_bottom" => 3,
                "margin_top" => 3,
    //            "margin_footer" => 0
        ]);
        
        $css = file_get_contents('../web/pdf/css/asistencia.css');
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);     
        $mpdf->Output();         
        return $this->redirect(Yii::$app->request->referrer);      
              
    }
    
     public function actionEtiquetas($ciclo,$modulo,$curso){
         $etiquetas = new SqlDataProvider([
                'sql' => "SELECT a.nombre nombre,a.apellidos apellidos, a.domicilio domicilio,
                                 a.localidad localidad, a.cp cp, a.provincia provincia FROM alumnos a join matriculas m on a.dni = m.dni_alumno join ciclos c on c.id = m.id_ciclo 
                   WHERE m.id_ciclo = $ciclo and m.curso = $curso",   
           ]); 
        
        
    
        $resultado =  $etiquetas->getModels();
       
        
           
            
           // $resultado_compara_modulos = $resultado_compara_modulos_ARR[0]['COMPLETO'];
            
                $filas = 0;
                $columnas = 0;
                $contenido = '
                            <font face="Georgia"><table>
                            <tr>';
                
                foreach ($resultado as $valor) {

                  $contenido .= '
                        <td align="left" style="width: 7cm;height: 1.3cm;"><p>'.$valor['nombre'].' '. $valor['apellidos'].'</p> 
                                         <p>'.$valor['domicilio'].'</p>
                                         <p>'.$valor['cp'].' - '. $valor['localidad'].'</p> 
                                          <p>'.$valor['provincia'].'</p> 
                        </td>';                      

                    if ($filas < 20){
                        if($columnas >=4){
                            $filas ++;
                            $columnas = 0;
                            $contenido .= '</tr><tr>';  
                        }
                    }
                    $columnas ++;
               }   
              
                $contenido .= '</tr></table>';
//                echo $contenido;
//                exit;
                  
                
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8','orientation' => 'P',
            "format" => "A4",
    //           
                "margin_bottom" => 0,
                "margin_top" => 0,
                "margin_left" =>1,
    //            "margin_footer" => 0
        ]);
        
       
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);     
        $mpdf->Output();         
        return $this->redirect(Yii::$app->request->referrer);      
              
    }
    
    
}