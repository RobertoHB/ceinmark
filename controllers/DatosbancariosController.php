<?php

namespace app\controllers;

use Yii;
use app\models\Datosbancarios;
use app\models\DatosbancariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\helpers\Json;

/**
 * DatosbancariosController implements the CRUD actions for Datosbancarios model.
 */
class DatosbancariosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Datosbancarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DatosbancariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Datosbancarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Datosbancarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $model = new Datosbancarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
             return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
//        return $this->render('create', [
//            'model' => $model,
//        ]);
    }

    /**
     * Updates an existing Datosbancarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Datosbancarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Datosbancarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Datosbancarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Datosbancarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
     public function actionDatosbanco($id)
    {
            $consulta = "SELECT banco,iban,titular,dni FROM datosbancarios WHERE id = $id";
            
            $dataProvider = new SqlDataProvider([
                    'sql' => $consulta,        
                ]);

            $resultado = $dataProvider->getModels();
         
        if(count($resultado)>0){
            foreach($resultado as $row){
                $datos = array('entidad'=>$row['banco'],'iban'=>$row['iban'],'dni'=>$row['dni'],'titular'=>$row['titular']);
                $datosJson = json_encode($datos);
                return $datosJson;
            
                                    
                  
            }
        }
        else{
            echo "";
        }
    }
    
    public function actionGuardardatosbancarios()
    {
       
        $model = new Datosbancarios();
        
        
//          if ($model->load(Yii::$app->request->post()) && $model->getMatriculaduplicada($alumno, $model->id_ciclo, $model->curso_academico)) {
//              
//          }
        $iban = $_POST["Datosbancarios"]["iban"];
        $compruebo_existe = $model->getDatosbancariosduplicado($iban);
        
       
        if ($model->load(Yii::$app->request->post()) &&  $compruebo_existe === 0) {
             $model->save();
            return($model->id);
        }elseif ($compruebo_existe !== 0){
            return($compruebo_existe);
        }
        
        
    
    }
    

    
    
}
