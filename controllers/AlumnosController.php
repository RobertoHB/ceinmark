<?php

namespace app\controllers;

use Yii;
use app\models\Alumnos;
use app\models\AlumnosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * AlumnosController implements the CRUD actions for Alumnos model.
 */
class AlumnosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumnos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlumnosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alumnos model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alumnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alumnos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dni]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Alumnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
     
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           
            return $this->redirect(['update', 'id' => $model->dni]);
      
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alumnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Alumnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alumnos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionGuardarimagen($dni){
     
     if(isset($_FILES['archivo'])){
        $ruta_origen = '../web/img/';
        $carpeta = '../web/img/alumnos/';
        $ruta = '../web/img/alumnos/'.$dni.'/';
        $ruta_foto = '../web/img/alumnos/'.$dni.'/personal/';
        $fichero_origen ='../web/img/alumnos/'.$dni.'/personal/Foto.png';
        
        if(!file_exists($carpeta))
            $ruta_alumno = mkdir($ruta_origen.'alumnos/',true);
       
        if(!file_exists($ruta))
            $ruta_nif = mkdir($carpeta.$dni.'/',true);   
            
        if(!file_exists($ruta_foto)){
            $carpeta_foto = mkdir($ruta.'personal/',true);
        }   
          
         //$fichero_subido = $ruta_foto . basename($_FILES['archivo']['name']);
        
        if (file_exists($fichero_origen)) {
            unlink($fichero_origen);
        }
        if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_origen)) {
           return $this->redirect(Yii::$app->request->referrer);
        } else {
            echo "¡Posible ataque de subida de ficheros!\n";
            return $this->redirect(Yii::$app->request->referrer);
        }

          
     }else{
         echo "no llega el fichero";
     }
        
        
    }
}
