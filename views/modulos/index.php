<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ModulosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modulos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modulos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Modulo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style'=>'width:80px;']
            ],
            'nombre',

//            ['class' => 'yii\grid\ActionColumn'],
            
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:80px;'],
                'header'=>'',
                'template' => '{actualizar}',
                'buttons' => [
                   'actualizar' => function ($url, $model, $key) {
                                                return  Html::a(
                                                '<span class="glyphicon glyphicon-pencil" style="padding-left:5px;"></span>',Url::to('@web/modulos/update'.'?id='.$model->id)                                                                
                                                        
                                                        );
                                },
                            ]
            
            ],
        ], 
                                                
    ]); ?>


</div>
