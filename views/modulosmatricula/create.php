<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modulosmatricula */

$this->title = 'Create Modulosmatricula';
$this->params['breadcrumbs'][] = ['label' => 'Modulosmatriculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modulosmatricula-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
