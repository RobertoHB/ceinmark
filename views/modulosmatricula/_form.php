<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Modulos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Modulosmatricula */
/* @var $form yii\widgets\ActiveForm */
$intemEstados = ['A'=>'Aprobado','NA'=>'No Aprobado','C'=>'Convalidada'];
$itemModulos = ArrayHelper::map(Modulos::find()->all(), 'id', 'nombre');   
?>

<div class="modulosmatricula-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    $form->field($model, 'id_matricula')->textInput()

    $form->field($model, 'id_modulo')->textInput() -->
      <?= $form->field($model, 'id_modulo') ->dropDownList($itemModulos, // Flat array ('id'=>'label')
                                       ['prompt'=>''])->label('Modulo'); ?>   

     <!--$form->field($model, 'estado')->textInput(['maxlength' => true])--> 
      <?= $form->field($model, 'estado') ->dropDownList($intemEstados, // Flat array ('id'=>'label')
                                       ['prompt'=>'']); ?>   
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
