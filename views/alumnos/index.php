<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlumnosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumnos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Alumno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            //'id_escolar',
            [
                'label'=>'Nº Escolar', 
                'attribute'=> 'id_escolar',
                 'headerOptions' => ['style' => 'width:50px;']
            ],
           // 'expe_centro',
            [
                'label'=>'Nº Exped.', 
                'attribute'=> 'expe_centro',
                 'headerOptions' => ['style' => 'width:50px;']
            ],
           // 'dni',
             [
                'label'=>'PASS/NIE', 
                'attribute'=> 'passnie',
                 'headerOptions' => ['style' => 'width:50px;']
            ],
           [
                'label'=>'DNI', 
                'attribute'=> 'dni',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
            //'nombre',
             [
                'label'=>'Nombre', 
                'attribute'=> 'nombre',
                 'headerOptions' => ['style' => 'width:150px;']
            ],
            'apellidos',
            //'f_nac',
            //'loc_nac',
            //'prov_nac',
            //'domicilio',
            //'cp',
            //'localidad',
            //'provincia',
            //'tel_fijo',
            //'movil',
            [
                'label'=>'Movil', 
                'attribute'=> 'movil',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
            //'discapacidad',
            //'centro_ant',
            //'tit_acceso',
            //'foto',
            'email:email',
            //'nacionalidad',
          

            ['class' => 'yii\grid\ActionColumn',
                 'contentOptions'=>['style'=>'width: 70px;'],
                
            ],
            
            
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{responsables}',
             'contentOptions'=>['style'=>'width: 40px;'],
                
             'buttons' => [
             'responsables' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, [
                                      'title' => Yii::t('app', 'Responsables'),
                                  ]);

                              }, 

                             ],

              'urlCreator' => function ($action, $model, $key, $index) {
                   return Url::to(['responsables/index', 'alumno' => $model->dni]);
              }        

            ],  
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{matriculas}',
             'contentOptions'=>['style'=>'width: 40px;'],
                
             'buttons' => [
             'matriculas' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-education"></span>', $url, [
                                      'title' => Yii::t('app', 'Matriculas'),
                                  ]);

                              }, 

                             ],

              'urlCreator' => function ($action, $model, $key, $index) {
                   return Url::to(['matriculas/index', 'alumno' => $model->dni]);
              }        

            ],  
            
        ],
    ]); ?>


</div>
