<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\Alumnos;
use yii\helpers\ArrayHelper;
use app\models\Discapacidades;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */
/* @var $form yii\widgets\ActiveForm */
$ruta_imagen_exist = '../web/img/alumnos/'.$model->dni.'/personal/foto.png';
$ruta_imagen_img = Url::to('@web/img/alumnos/'.$model->dni.'/personal/foto.png');
$itemDiscapacidades = ArrayHelper::map(Discapacidades::find()->all(), 'id', 'tipo');
?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/firma.css')?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<script src="<?= Url::to('@web/js/firma_electronica.js')?>"></script>




<div class="alumnos-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="form-group row">
        <div class="col col-sm-6" style="border: 1px solid #DFEEF7;border-radius: 10px;height: 215px;width:570px;margin-left:15px;">
            
            <div class="col-sm-8" style="padding-top:20px;">
                <?= $form->field($model, 'id_escolar')->textInput() ?>
                <?= $form->field($model, 'expe_centro')->textInput() ?>
                
            </div>
            <div class="col-sm-4" style="padding-top:20px;">
              <?=  file_exists ($ruta_imagen_exist) ? '<img src="'.$ruta_imagen_img.'" id="img_ficha" width="80%" align="center" 
                                        style="border-radius: 10px"/>' : 'Foto' ?>   
            </div>
          
        </div>        
        <div class="col col-sm-6">
              <div class="col col-sm-2">
                <?= $form->field($model, 'passnie')->textInput(['maxlength' => true]) ?>
              </div>
              <div class="col col-sm-10">
                <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>
              </div>
          
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
           
        </div>    
    </div><hr>
     <div class="form-group row">
        <div class="col col-sm-6">
             <div class="col col-sm-6">
                <?= $form->field($model, 'f_nac')->widget(DatePicker::className(), [
               // inline too, not bad
                   'inline' => false, 
                    // modify template for custom rendering
                   //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                   'clientOptions' => [
                   'autoclose' => true,
                   'format' => 'dd-mm-yyyy',
                   'todayBtn' => true
                   ]
                ]);?>
             </div>
             <div class="col col-sm-6">
                <?= $form->field($model, 'loc_nac')->textInput(['maxlength' => true]) ?>
             </div>    
             <div class="col col-sm-6">
                <?= $form->field($model, 'prov_nac')->textInput(['maxlength' => true]) ?>
             </div>
             <div class="col col-sm-6">
                <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?>
             </div>       
              <?= $form->field($model, 'discapacidad')->dropDownList($itemDiscapacidades, ['prompt' => 'Seleccione Uno' ]); ?>
        </div>
          
            <div class="col col-sm-6">
                <?= $form->field($model, 'domicilio')->textInput(['maxlength' => true]) ?>
          
                <div class="col-sm-3">
                    <?= $form->field($model, 'cp')->textInput() ?>
                </div>
                <div class="col-sm-9">
                    <?= $form->field($model, 'localidad')->textInput(['maxlength' => true]) ?>
                </div>   
           
                <?= $form->field($model, 'provincia')->textInput(['maxlength' => true]) ?>
              
            </div>     
            
     </div><hr>     
     <div class="form-group row">
        <div class="col col-sm-6">  
              <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
             <div class="col col-sm-6"> 
                 <?= $form->field($model, 'tel_fijo')->textInput() ?>
             </div>
             <div class="col col-sm-6"> 
                 <?= $form->field($model, 'movil')->textInput() ?>
             </div>    
          
        </div>
         <div class="col col-sm-6">  
            <?= $form->field($model, 'centro_ant')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'tit_acceso')->textInput(['maxlength' => true]) ?>
           
         </div>    
     </div> <hr>    
    <div class="form-group row">
        <div class="form-group" style="border:1px solid grey;border-radius: 5px;height: 25px;width:550px;margin:10px;height: 28px;">
            <div class="col-sm-4">
                <?= $form->field($model, 'f_proteccion')->checkbox(['label' => 'Protección Datos', 'class'=>'form-check-input','id'=>'f_proteccion'])->label('') ?>
            </div>    
            <div class="col-sm-4">
              
            </div>

        </div>   

        </div>
       

            
     
     
     
     
     
     
    <!--$form->field($model, 'foto')->fileInput(['maxlength' => true,'capture'=>"camera",'style'=>""])->label(false);-->    
<!--    $form->field($model, 'imgcif')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen CIF",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagencif']])->label(false);-->
     
    
    

    <div class="form-group row">
        <div class="col-sm-6">
            <div class="col-sm-3">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), 
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>    
            <div class="col-sm-3">
                <?= Html::a('Matriculas', ['/matriculas/index','alumno' => $model->dni], ['class' => 'btn btn-success']);?>
               
            </div>    
            <div class="col-sm-3">
                 <?= Html::Button('Foto', ['class' => 'btn btn-success','id'=>'foto']);?>
            </div>    
            <div class="col-sm-3">
                
            </div>
            
        </div>
        <div class="col-sm-6">
        </div>    
    <?php ActiveForm::end(); ?>

</div>


<div class="form-group row">
        <div class="col-sm-12">
            <?php //yii\widgets\Pjax::begin(['id' => 'form_add_modulo']) ?>
                <?php $form = ActiveForm::begin([
                     'method' => 'post',
                     'action' => ['guardarimagen','dni'=>$model->dni],
                    'options' => ['id' => 'foto_alumno','enctype' => 'multipart/form-data']]);
               
                ?> 

            <input type="file" accept="image/*;capture=camera" id="foto_al" name="archivo" style="display: none;" />
                    
                     <?= Html::submitButton('Enviar', ['class' => 'btn btn-success','id'=>'envio_imagen','style'=>'display:none']);?>

         </div>   
               <?php ActiveForm::end();?>

</div>


<script>
$( document ).ready(function() {
    $('#foto').click(function(event) { 
        
       $('#foto_al').click();
            
    });
     $('#foto_al').change(function(event) { 
//       
      $('#envio_imagen').click();
      
//            
    });
     
}); 
</script>