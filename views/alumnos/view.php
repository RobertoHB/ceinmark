<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alumnos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_escolar',
            'expe_centro',
            'passnie',
            'dni',
            'nombre',
            'apellidos',
            'f_nac',
            'loc_nac',
            'prov_nac',
            'domicilio',
            'cp',
            'localidad',
            'provincia',
            'tel_fijo',
            'movil',
            'discapacidad',
            'centro_ant',
            'tit_acceso',
            'foto',
            'email:email',
            'nacionalidad',
           
        ],
    ]) ?>

</div>
