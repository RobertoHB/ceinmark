<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Ceinmark';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
	<title>Ceinmark</title>
	
	<style type="text/css">
		body{
			background-color: #24529F;
			
		}
		input {
			position: absolute; 
			display: none;
		}
		.boton{
			width: 120px;
			height: 36px;
			border: 2px solid white;
			border-radius: 6px;
			background-color: white;
			color: #24529F;
			text-align: center;
			font-weight: bold;
			font-size: large;
		}
		.btn{
			cursor: pointer;
			position: absolute;
			display: flex;
			justify-content: center;
			align-items: center;
			transition: all 0.3s cubic-bezier(.25,.8,.25,1);
		}
		.menu {
			margin: 0 auto;
			position: absolute;
			top: 50%;
			left: 50%;
			margin-left: -25px;
			margin-top: -25px;
		}
		.btn:not(:first-child) {
			opacity: 0;
			z-index: -2;  
			transition: all 0.6s cubic-bezier(.87,-.41,.19,1.44);
		}
		.btn:nth-child(2) {
			top:0px;
			-webkit-transition-delay: 0s;
			transition-delay: 0s;
		}
		.btn:nth-child(3) {
			top:0px;
			left:0px;
			-webkit-transition-delay: 0.1s;
			transition-delay: 0.1s;
		}
		.btn:nth-child(4) {
			left:0px;
			-webkit-transition-delay: 0.2s;
			transition-delay: 0.2s;
		}
		.btn:nth-child(5) {
			top:0px;
			left:0px;
			-webkit-transition-delay: 0.3s;
			transition-delay: 0.3s;
		}
		.btn:nth-child(6) {
			top:0px;
			-webkit-transition-delay: 0.4s;
			transition-delay: 0.4s;
		}
		input#toggle:checked ~ #show-menu .btn:nth-child(2) {
		  top:-150px;
		  left:-40px;
		  opacity:1;
		}
		input#toggle:checked ~ #show-menu .btn:nth-child(3) {
		  top:-46px;
		  left:162px;
		  opacity:1;
		}
		input#toggle:checked ~ #show-menu .btn:nth-child(4) {
		  top:121px;
		  left:88px;
		  opacity:1;
		}
		input#toggle:checked ~ #show-menu .btn:nth-child(5) {
		  top:121px;
		  left:-158px;
		  opacity:1;
		}
		input#toggle:checked ~ #show-menu .btn:nth-child(6) {
		  top:-46px;
		  left:-242px;
		  opacity:1;
		}
		.menuBtn, .closeBtn {
			position: absolute;
			transition: all 0.3s ease;
		}
		.closeBtn {
			transform: translateY(50px);
			opacity: 0;
		}
		input#toggle:checked ~ #show-menu .btn .menuBtn {
			transform: translateY(-50px);
			opacity: 0;
		}
		input#toggle:checked ~ #show-menu .btn .closeBtn {
			transform: translateY(0px);
			opacity: 1;
		}
	</style>
</head>
<body> 
    <main>
        
        <div class="container">
           
            <div class="row">
                
                <div class="menu">
                    
                    <input type="checkbox" id="toggle"/>
                     
                    <label id="show-menu" for="toggle">
                        
                    <div class="btn">
                        <img class="toggleBtn menuBtn" src="<?= Url::to('@web/img/logoPie.jpg')?>"/>
                        <img class="toggleBtn closeBtn" src="<?= Url::to('@web/img/logoPie.jpg')?>">close                                    
                    </div>
                        
                    <div class="btn" id="alumnos">
                        <button type="button" class="boton" id="alumnos" >Alumnos</button>  
                    </div>
                    <div class="btn">
                            <button type="button" class="boton" id="matriculas" >Matrículas</button>	
                    </div>
                    <div class="btn">
                            <button type="button" class="boton" id="ciclos" >Ciclos</button>
                    </div>
                    <div class="btn">
                            <button type="button" class="boton" id="modulos" >Módulos</button>
                    </div>
                    <div class="btn">
                            <button type="button" class="boton" id="informes" >Informes</button>
                    </div>
                    </label>
                </div>
            </div>
        </div>
    </main>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</html>

<script>
    
 $( document ).ready(function() {  
       $("[class='boton']").on('click', function(){
        console.log($(this).attr('id'));
      })
  
 });
</script>

