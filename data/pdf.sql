USE matriculas;
SHOW TABLES;

SELECT * FROM matriculas m;
SELECT * FROM alumnos a;

SELECT a.nombre nombre,a.apellidos apellidos, a.f_nac fechanaci,a.domicilio domicilio,a.cp cp,
       a.localidad localidad, a.tel_fijo fijo, a.movil movil,a.email email,
       c.denominacion denominacion,m.curso curso,m.curso_academico cursoacademico 
            FROM matriculas m JOIN alumnos a ON m.dni_alumno = a.dni 
                              JOIN ciclos c ON m.id_ciclo = c.id 
                              WHERE m.id = '82';

SELECT * FROM responsables r;

SELECT * FROM tutores t;
SELECT * FROM responsables r;

SELECT t.nombre nombreTutor,t.apellidos apeltutor,t.dni dni,t.telefono telef FROM alumnos a JOIN responsables r ON
  a.dni = r.dni_alumno JOIN
  tutores t ON t.id = r.id_tutor WHERE a.dni = '20195088F'