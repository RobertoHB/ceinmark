   
window.addEventListener("load",(e)=>{

$(document).on("click", "#bot_modal_firma", function () {
     
      $('#contenedorCanvas').show();
        


let contenedor_canvas = document.querySelector('#contenedorCanvas');

contenedor_canvas.style = "top:25%;left:35%;display:inline-block;opacity:1;z-index:100;background-color: white;Max-Width: 100%;overflow: hidden;position: absolute;border:3px solid #1A596C;border-radius:5%";

   //VAriables para funcionalidad canvas firma electronica
 let canvas;
 let ctx;
 let borrarLienzo = document.querySelector("#borrar");
 let confirmarLienzo = document.querySelector("#confirmar");
 let validarLienzo = document.querySelector("#validar");
 let contenedor_elemento_img = document.querySelector("#contenedor_imag");
 let cerrar_ventana_firmas = document.querySelector("#cerrar_firma");
  //---------------------------------------------------------------

canvas = document.querySelector("canvas");
ctx = canvas.getContext("2d");
ctx.strokeStyle = "#222222";
ctx.lineWidth = 4;
// Set up mouse events for drawing
var drawing = false;
var mousePos = { x:0, y:0 };
var lastPos = mousePos;
canvas.addEventListener("mousedown", function (e) {
       drawing = true;
 lastPos = getMousePos(canvas, e);
}, false);
canvas.addEventListener("mouseup", function (e) {
 drawing = false;
}, false);
canvas.addEventListener("mousemove", function (e) {
 mousePos = getMousePos(canvas, e);
}, false);

// Get the position of the mouse relative to the canvas
function getMousePos(canvasDom, mouseEvent) {
 var rect = canvasDom.getBoundingClientRect();
 return {
   x: mouseEvent.clientX - rect.left,
   y: mouseEvent.clientY - rect.top
 };
}

// Get a regular interval for drawing to the screen
   window.requestAnimFrame = (function (callback) {
   return window.requestAnimationFrame || 
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimaitonFrame ||
      function (callback) {
   window.setTimeout(callback, 1000/60);
      };
   })();


// Draw to the canvas
function renderCanvas() {
 if (drawing) {
   ctx.moveTo(lastPos.x, lastPos.y);
   ctx.lineTo(mousePos.x, mousePos.y);
   ctx.stroke();
   lastPos = mousePos;
 }
}

// Allow for animation
(function drawLoop () {
 requestAnimFrame(drawLoop);
 renderCanvas();
})();

// Set up touch events for mobile, etc
canvas.addEventListener("touchstart", function (e) {
       mousePos = getTouchPos(canvas, e);
 var touch = e.touches[0];
 var mouseEvent = new MouseEvent("mousedown", {
   clientX: touch.clientX,
   clientY: touch.clientY
 });
 canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchend", function (e) {
 var mouseEvent = new MouseEvent("mouseup", {});
 canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchmove", function (e) {
 var touch = e.touches[0];
 var mouseEvent = new MouseEvent("mousemove", {
   clientX: touch.clientX,
   clientY: touch.clientY
 });
 canvas.dispatchEvent(mouseEvent);
 e.preventDefault();
}, false);

// Get the position of a touch relative to the canvas
function getTouchPos(canvasDom, touchEvent) {
 var rect = canvasDom.getBoundingClientRect();
 return {
   x: touchEvent.touches[0].clientX - rect.left,
   y: touchEvent.touches[0].clientY - rect.top
 };
}

// Prevent scrolling when touching the canvas
document.body.addEventListener("touchstart", function (e) {
 if (e.target == canvas) {
   e.preventDefault();
 }
}, false);
document.body.addEventListener("touchend", function (e) {
 if (e.target == canvas) {
   e.preventDefault();
 }
}, false);
document.body.addEventListener("touchmove", function (e) {
 if (e.target == canvas) {
   e.preventDefault();
 }
}, false);


//Escuchadores de eventos para firma electronica

//Borramos el canvas y eliminamos el elemento de imagen que guarda el mismo
borrarLienzo.addEventListener('click', (e)=>{
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  canvas.width = canvas.width;
  ctx.lineWidth = 4;
  let elementBorrar = document.querySelector("#imagen_validada");
  elementBorrar.parentNode.removeChild(elementBorrar);


});
//guardamos el dibujo del canvas en un elemento de imagen creado previamente al validar el canvas
confirmarLienzo.addEventListener('click', (e)=>{
 //mostrar_img.src = canvas.toDataURL("image/png"); 

   document.querySelector('#contenedorCanvas').style.display = "none";
   $("#f_matricula").prop('checked', true);

});


//validamos el canvas como firma correcta y creamos el elemento img en ese momento para despues mostrar el resultado en el mismo y poder guardar la imagen en el servidor. 
validarLienzo.addEventListener('click', (e)=>{

//crear elemento img
let newElement = document.createElement("img");
contenedor_elemento_img.appendChild(newElement);

 let newElemento_img = document.querySelector("img");
 newElemento_img.setAttribute("id", "imagen_validada"); 
 newElemento_img.classList.add("img-fluid");
  newElemento_img.style.width = "80%";
 document.querySelector("#confirmar").style.display ="inline";
 
 $('#contenedor_imag').show();

//asignar imagen canvas en en atributo src de la imagen

 newElemento_img.src = canvas.toDataURL("image/png");
   //console.log(btoa(newElemento_img));
   //console.log(newElemento_img.src);
 $('#firma_matricula').val(newElemento_img.src);



});

cerrar_ventana_firmas.addEventListener('click', (e)=>{

   document.querySelector('#contenedorCanvas').style.display = "none";
  
 
//    contenedor_elemento_img.remove( "contenedor_elemento_imgimg" );

});


});






});

